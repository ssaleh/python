#db03.py
import MySQLdb
aConnection = MySQLdb.connect(
host = "localhost", db='Test', user='student', passwd='password')
aCursor = aConnection.cursor()
#aCursor.execute('''DROP TABLE IF EXISTS Demographics;''')
aCursor.execute('''CREATE TABLE Demographics
                   (  studentID     INT NOT NULL,
                      firstname     VARCHAR(20),
                      middleinitial VARCHAR(3),
                      lastname      VARCHAR(35),
                      streetaddress VARCHAR(30),
                      city          VARCHAR(25),
                      state         VARCHAR(2),
                      phone         VARCHAR(25),
                      PRIMARY KEY (studentID)
                    );
               ''')     
aConnection.commit()
aConnection.close()
