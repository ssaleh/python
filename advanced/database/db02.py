#!/usr/bin/python
#db02.py
import MySQLdb
myConnection = MySQLdb.connect(user='root',passwd='password')
myConnection.query('CREATE DATABASE IF NOT EXISTS Test;')
myConnection.query("GRANT ALL ON Test.* to 'student'@'localhost' IDENTIFIED BY 'password';")
myConnection.commit()
myConnection.close()
myConnection = MySQLdb.connect(
host='localhost', user='student', passwd='password', db='Test')
