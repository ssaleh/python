#!/usr/bin/python
# db12.py
# An example of using an Object Relational Manager
# http://www.sqlalchemy.org/
# SQLAlchemy will be used to relate 
# object instances to database records
try:
    import sqlalchemy
except ImportError, e:
    print 'Visit http://www.sqlalchemy.org/ for information about the sqlalchemy module', e
    import sys
    sys.exit(1)
    
# provided by python-sqlalchemy in Fedora
engine = sqlalchemy.create_engine('sqlite:///users.db', echo=True)
metadata = sqlalchemy.MetaData()
userTable = sqlalchemy.Table('Users', metadata,
     sqlalchemy.Column('Username', sqlalchemy.Text, primary_key=True),
     sqlalchemy.Column('Password', sqlalchemy.Text)
)
metadata.create_all(engine) 
# define a class to map to the Table
class User(object):
     def __init__(self, name, password):
         self.Username = name
         self.Password = password
     def __repr__(self):
        return "Username: '%s', Password: '%s'" % (self.Username, self.Password)

# map the Object to the Table
from sqlalchemy.orm import mapper    
mapper(User, userTable)

# create a session
from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind=engine)
session = Session()

# create an instance of the User object
aUser = User('Chip', 'und3rd0g')

# store records in the database
session.add(aUser)
session.add_all([
     User('Tommy', '$uperbad'),
     User('Poncho',  'L0L0m6')])
    
aRecord = session.query(User).first() 
print aRecord
allRecords = session.query(User).all()
print allRecords
session.commit()
