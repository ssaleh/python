#db07.py
import MySQLdb
import sys

try:
    aConnection = MySQLdb.connect(
    host = "localhost", db='Test', user='student', passwd='password')
    aCursor = aConnection.cursor()
    aCursor.execute ("SELECT * FROM Demographics")
    print 'This is the data now in Students table prior to the UPDATE:'
    for row in aCursor.fetchall():
        print row
    print    
    aCursor.execute ("UPDATE Demographics SET state='OR' WHERE state ='CA'")
    aCursor.execute ("SELECT * FROM Demographics WHERE state='OR'")
    print 'This is the data now in Students table after the UPDATE:'
    for row in aCursor.fetchall():
        print row
    print    
    aCursor.close()
    aConnection.commit()
    aConnection.close()
except MySQLdb.Error, e:
     print "Error %d: %s" % (e.args[0], e.args[1])
     sys.exit (1)
