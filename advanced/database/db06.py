#db06.py
import sys
import MySQLdb

try:
    aConnection = MySQLdb.connect(
    host = "localhost", db='Test', user='student', passwd='password')
    aCursor = aConnection.cursor()
    aCursor.execute ("SELECT * FROM Demographics")
    for row in aCursor.fetchall():
        print row
    aCursor.close()
    aConnection.commit()
    aConnection.close()

except MySQLdb.Error, e:
     print "Error %d: %s" % (e.args[0], e.args[1])
     sys.exit (1)
