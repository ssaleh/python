#!/usr/bin/python
#db04.py
import MySQLdb
aConnection = MySQLdb.connect(
host = "localhost", db='Test', user='student', passwd='password')
aCursor = aConnection.cursor()
aCursor.execute('''CREATE TABLE CourseRecord
                   (  studentID     INT NOT NULL,
                      Dept          VARCHAR(6),      
                      CourseID      INT,            
                      Section       INT,
                      Semester      VARCHAR(6),
                      YEAR          INT,
                      GradeEarned   CHAR(1),
                      Transferable  CHAR(1),
                      KEY (studentID)
                    )''')     
aCursor.close()
aConnection.commit()
aConnection.close()                      
