#!/usr/bin/python
#db01.py
import MySQLdb
myConnection = MySQLdb.connect(user='root',passwd='password')
myConnection.query('DROP DATABASE IF EXISTS Test;')
myConnection.query('CREATE DATABASE Test;')
myConnection.query("GRANT ALL ON Test.* to student@'localhost';")
myConnection.commit()
myConnection.close()
