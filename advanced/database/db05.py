#db05.py

import MySQLdb
aConnection = MySQLdb.connect(
host = "localhost", db='Test', user='student', passwd='password')
aCursor = aConnection.cursor()
aCursor.execute ("""
       INSERT INTO Demographics
       (studentID, firstname,  middleinitial, lastname, streetaddress, city, state, phone)
       VALUES
         (543677890, 'John',   'J', 'Jones',    '567 3/4 BlueBerry Ln', 'Bellewood', 'CA', '6194554321'),
         (543677891, 'Joseph', 'R', 'Rourke',   '991 1/2 Razzbury Ln',  'Bellewood', 'CA', '6194556219'),
         (543677892, 'Joan',   'A', 'Ardvark',  '899 Blackberry Ln',    'Bellewood', 'CA', '6194554541'),
         (543677893, 'Jim',    'Z', 'Zohara',   '123 Boysenerry Ln',    'Bellewood', 'CA', '6194554444')
     """)
print "Number of rows inserted: %d" % aCursor.rowcount
aCursor.close()
aConnection.commit()
aConnection.close()
