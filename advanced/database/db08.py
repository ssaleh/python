#db08.py
import MySQLdb
import sys

try:
    aConnection = MySQLdb.connect(
    host = "localhost", db='Test', user='student', passwd='password')
    aCursor = aConnection.cursor()
    aCursor.execute ("SELECT * FROM Demographics")
    print 'This is the data now in Demographics table prior to the DELETE:'
    for row in aCursor.fetchall():
        print row
    print    
    aCursor.execute ('''DELETE FROM Demographics WHERE firstname ='Joan'
    AND lastname='Ardvark' ''')
    aCursor.execute ("SELECT * FROM Demographics")
    print 'This is the data now in Demographics table after the UPDATE:'
    for row in aCursor.fetchall():
        print row

    aCursor.close()
    aConnection.commit()
    aConnection.close()
except MySQLdb.Error, e:
     print "Error %d: %s" % (e.args[0], e.args[1])
     sys.exit (1)
