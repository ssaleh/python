#queue1.py
# more general threading example has threads doing two different jobs
# note that the functions list shows we can add more jobs for the threads
# generates a random number of threads each time the program is run
from time import ctime, sleep
from threading import Thread
from random import randint as getRandomInterger
from Queue import Queue

class AnotherThread(Thread):
    def __init__(self, workToDo, workArgs, name=''):
        Thread.__init__(self)
        self.workToDo = workToDo
        self.workArgs = workArgs
        self.name = name
    def run(self):
        apply(self.workToDo, self.workArgs)
 
#create one army and add it to the Queue
def createAnArmy(queueOfArmies):
    try:
        print '\nAdding new army to the Fort' 
        queueOfArmies.put('Army',1)
        print '\nAfter adding it, there are now', queueOfArmies.qsize(), 'armies in the Fort'
    except:
        print "\noops! Could not add this army to the Fort!\n"
        
#get one army from the Queue
def getAnArmy(queueOfArmies):
    try:
        print '\nNow getting a new army from the Fort' 
        if not queueOfArmies.empty():
            army = queueOfArmies.get(1)
        print '\nAfter removing this one, there are now', queueOfArmies.qsize(), ' armies left in the Fort'
    except:
        print "\nCould not remove an army from the Fort because there are only", queueOfArmies.qsize(), "armies there"

#Continued on the next page
 
def armyBuilder(armiesQ, howMany):
    for x in range(howMany):
        createAnArmy(armiesQ)
        sleep(getRandomInterger(1,3)) #randomly sleep for 1,2, or 3 seconds

def armyUser(armiesQ, howMany):
    for x in range(howMany):
        getAnArmy(armiesQ)
        sleep(getRandomInterger(3,6)) #randomly sleep for 3, 4, 5, 6 seconds


listofThreadFunctions = [armyBuilder, armyUser]
indexThreadFunctions = range(len(listofThreadFunctions))

if __name__ == "__main__":
    print 'start program', ctime()
    #create random number of threads; from 5 to 15 created 
    numberOfThreads = getRandomInterger(5,15) 
    someArmyQ = Queue(32)  #create a Queue to hold up to 32 armies
    listOfThreads = []
    
    #create a list of threads 
    for x in indexThreadFunctions:
       gotAThread = AnotherThread(listofThreadFunctions[x],(someArmyQ,numberOfThreads),listofThreadFunctions[x].__name__) 
       listOfThreads.append(gotAThread)

    #start new threads  
    for x in indexThreadFunctions:
        listOfThreads[x].start()

    #make threads block until all are done
    for x in indexThreadFunctions:
        listOfThreads[x].join() 
    print 'end program', ctime()
