#nothread02.py
from time import ctime, sleep

def threadOneDoesThis(talking,napTime):
    while 1:
        print talking
        print 'start thread 1: ', ctime()
        sleep(napTime)
        print 'stop thread 1: ', ctime()
        break
def threadTwoDoesThis(talking, napTime):
     while 1:
         print talking
         print 'start thread 2: ', ctime()
         sleep(napTime)
         print 'stop thread 2: ', ctime()
         break

#Continued on the next page
 
def threadThreeDoesThis(talking, napTime):
     while 1:
         print talking
         print 'start thread 3: ', ctime()
         sleep(napTime)
         print 'stop thread 3: ', ctime()
         break
def threadFourDoesThis(talking, napTime):
     while 1:
         print talking
         print 'start thread 4: ', ctime()
         sleep(napTime)
         print 'stop thread 4: ', ctime()
         break        

if __name__ == "__main__":
    print 'start program', ctime()
    threadOneDoesThis("Thread 1", 6)
    threadTwoDoesThis("Thread 2", 3)
    threadThreeDoesThis("Thread 3", 2)
    threadFourDoesThis("Thread 4", 4)
    print 'end program', ctime()
