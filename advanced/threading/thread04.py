#thread04.py

class iAmCallable(object):
    def __call__(self,*args):
        print "callable object called with args: ", args
         
if __name__ == "__main__":
    #create a few of these objects
    oneCall = iAmCallable()
    secCall = iAmCallable()
    thirdCall = iAmCallable()
    #call them as if they were functions
    oneCall(344,555,56,'exit')
    secCall('the end is near', 'drink root beer')
    thirdCall(45.66/5)
    oneCall([4,5,6,88.1])
