#threading3.py
from time import ctime, sleep
import threading

listOfNapTimes = [6,3,2,4]

class allThreadsDoThis(object):
    def __init__(self, workToDo, workArgs, name=''):
        self.workToDo = workToDo
        self.workArgs = workArgs
        self.name = name

    def __call__(self):
        apply(self.workToDo, self.workArgs)

def ThreadWork(whichThread,howLong):
    try:
        print 'start of DoThis: ', whichThread, 'time is:', ctime(), '\n'
        sleep(howLong)
        print 'end of DoThis: ', whichThread, 'time is:' , ctime(), '\n'
    except:
        print "oops! This thread: ", whichThread, "how long?",  howLong, '\n'
 
if __name__ == "__main__":
    print 'start program', ctime()
    listOfThreads = []
    listOfThreadIndex = range(len(listOfNapTimes)) #converts into list [0,1,2,3]
       
    #create a list of threads; we don't need to set up our own locks
    for x in listOfThreadIndex:
       gotAThread = threading.Thread(target=allThreadsDoThis(ThreadWork,(x,listOfNapTimes[x]),ThreadWork.__name__))
       listOfThreads.append(gotAThread)
        
    #start new threads  
    for x in listOfThreadIndex:
         listOfThreads[x].start() 
    #make threads block until all are done
    for x in listOfThreadIndex:
        listOfThreads[x].join() 
        
    print 'end program', ctime()
