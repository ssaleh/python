#locking1.py
from time import ctime, sleep
import thread

#listOfNapTimes = [6,3,2,4] 

def allThreadsDoThis(talking, howLong, myLock):
    try:
        myLock.acquire()
        print 'Got a lock for ', talking, 'and it will last for: ', howLong, 'seconds', 'time is:' , ctime(), '\n' 
        sleep(howLong)
        print 'Will release a lock for ', talking, 'time is:' , ctime(), '\n'
        myLock.release()
    except:
        print "oops! This thread: ", talking, "cannot run for ",   howLong, '\n'

 
if __name__ == "__main__":
    print 'start program\n', ctime(), '\n'
    myLock = thread.allocate_lock()
    thread.start_new_thread(allThreadsDoThis,(" Thread 1 ", 6, myLock ))
    thread.start_new_thread(allThreadsDoThis,(" Thread 2 ", 3, myLock ))
    thread.start_new_thread(allThreadsDoThis,(" Thread 3 ", 2, myLock ))
    thread.start_new_thread(allThreadsDoThis,(" Thread 4 ", 4, myLock ))        
    print 'end program\n', ctime(), '\n'
