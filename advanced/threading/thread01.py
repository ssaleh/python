#thread01.py
import time
import thread

def threadOneDoesThis(talking,napTime):
    while 1:
        print talking
        time.sleep(napTime)
        break
def threadTwoDoesThis(talking, napTime):
     while 1:
         print talking
         time.sleep(napTime)
         break
#notice the command after the function name before its args
if __name__ == "__main__":
    thread.start_new_thread(threadOneDoesThis,("Thread 1", 4))
    thread.start_new_thread(threadTwoDoesThis,("Thread 2", 3))
