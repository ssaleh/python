#threading1.py
from time import ctime, sleep
import threading
listOfNapTimes = [6,3,2,4]

def allThreadsDoThis(whichThread, howLong):
    try:
        print 'start of DoThis: ', whichThread, 'time is:', ctime(), '\n'
        sleep(howLong)
        print 'end of DoThis: ', whichThread, 'time is:' , ctime(), '\n'
    except:
        print "oops! This thread: ", whichThread, "how long?",  howLong, '\n'
 
if __name__ == "__main__":
    print 'start program', ctime()
    listOfThreads = []
    listOfThreadIndex = range(len(listOfNapTimes)) #converts into list [0,1,2,3]
#Continued on the next page
 
    #create a list of threads; we don't need to set up our own locks
    for x in listOfThreadIndex:
        gotAThread = threading.Thread(target=allThreadsDoThis,
        args=(x,listOfNapTimes[x]))
        listOfThreads.append(gotAThread)
     
    #start new threads  
    for x in listOfThreadIndex:
         listOfThreads[x].start() 
    #make threads block until all are done
    for x in listOfThreadIndex:
        listOfThreads[x].join() 
    print 'end program', ctime()
