#thread06.py
from threading import Thread

#the init method MUST call the parent class init method to use it
class iAmAThreadToo(Thread):
    def __init__(self,num):
        Thread.__init__(self)
        self.num = num

    def run(self):
        for x in range(2,10,2):
            for y in range(self.num):
                result = 0
                for z in range(self.num):
                    result += 1
                    print result
if __name__ == "__main__":
    oneThread = iAmAThreadToo(10)
    oneThread.start()
    oneThread.join()
