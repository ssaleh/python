#condition01.py
from time import sleep, ctime
from threading import *
import sys
from random import randint

#Continued on the next page
 
#create a simple functional data structure that acts as a Queue
class myOwnQ:
    def __init__(self, size=10):
        self.total=0
        self.maxSize=size
    def addToCount(self,number=1):
        if not self.isFull():
            self.total+=number
        else:
            print "cannot add to Queue"
    def subtractFromCount(self):
        if self.total:
            self.total -=1
    def isEmpty(self):
        return not self.total
    def isFull(self):
        if not self.total < self.maxSize:
            return True
        else:
            return False
        
#Continued on the next page
 
#create a producer Thread
class AddToHealthPoints(Thread):
    def __init__(self,testCond,myQObject,napHowLong=1):
        Thread.__init__(self)
        self.condn = testCond
        self.myQObj = myQObject
        self.nap=napHowLong
    def run(self):  #what the thread does
        while True:
            randStop = randint(1,5)
            if randStop == 5:
                stopYes = raw_input("Do you want to stop?  Y or N")
                if stopYes == 'Y' or stopYes == 'y':
                    print "thank you for using our health care service. Bye!"
                    sys.exit()
            self.condn.acquire()   #currentThread is ID of currently running Thread
            while self.myQObj.isFull():#wait until there is room
                self.condn.wait()
            print currentThread(), "\nI added some Health Points"
            self.myQObj.addToCount()
            self.condn.notifyAll()
            self.condn.release()
            sleep(self.nap)
#Continued on the next page
 
#create a consumer Thread        
class UseHealthPoints(Thread):
    def __init__(self,testCond,myQObject,napHowLong=1):
        Thread.__init__(self)
        self.condn = testCond
        self.myQObj = myQObject
        self.nap=napHowLong
    
    def run(self):
         while True:
            sleep(self.nap)
            self.condn.acquire()
            while self.myQObj.isEmpty():#wait until there is data 
                self.condn.wait()
            self.myQObj.subtractFromCount() #take some health
            print currentThread(), "\nI am sick so I used some Health Points"
            self.condn.release()

#Continued on the next page           
 
if __name__ == "__main__":
    print 'start program', ctime()
    hospital = myOwnQ()  #holds health points
    testCondition = Condition()  #class from threading.py
    
    doctor = AddToHealthPoints(testCondition, hospital) #producer
    patient1 = UseHealthPoints(testCondition, hospital) #consumer
    patient2 = UseHealthPoints(testCondition, hospital) #consumer
    patient3 = UseHealthPoints(testCondition, hospital) #consumer

    doctor.start()
    patient1.start()
    patient2.start()
    patient3.start()
    print 'end   program', ctime()
