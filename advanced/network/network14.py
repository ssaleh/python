#!/usr/bin/python
# network14.py

# create a TCP Server that will start up, and wait for a client to connect
# using the SocketServer module's class rather than socket functions

from SocketServer import TCPServer, StreamRequestHandler
from time import ctime

host = ''
port = 10001
AddressWhere = (host,port)

class TimeRequestHandler(StreamRequestHandler):
    def handle(self):
        print '...Client is connected at: ', self.client_address
        data = self.rfile.readline()
        self.wfile.write('[%s] %s' %(ctime(), data))
#Continued on the next page
 
MyTCPServer = TCPServer(AddressWhere, TimeRequestHandler)
print 'Server is waiting for Client connection...'
MyTCPServer.serve_forever()

