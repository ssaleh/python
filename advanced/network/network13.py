#!/usr/bin/python
#network13.py
'''Example module for ftp
 Imagine you want mozilla's sunbird calendar product for win32
'''

import ftplib
import socket
import sys

HOST = "192.168.1.118"
DIR = "/pub/"
FILE = "README"

try:
    myFTPObj = ftplib.FTP(HOST)
except:
    print "cannot get ftp address %s " % HOST
else:
    print "got to ftp site %s " % HOST

#Continued on the next page
 
try:
    myFTPObj.login()    #see if can login anon
except:
    print "cannot login in anonymously"
else:
    print "connected to" , HOST

try:
    myFTPObj.cwd(DIR)
except:
    print "cannot change directory to %s " % DIR
else:
    print "changed to directory %s " % DIR
    print "working directory is: ", myFTPObj.pwd()
    print "the files here are only 2: "
    print myFTPObj.dir()

try:
#    myFTPObj.retrcmd("get README")
    filename = 'README'
    outfile = sys.stdout
    myFTPObj.retrlines("RETR " + filename, lambda s, w=outfile.write: w(s+"\n"))

except:
    print 'error getting README'
else:
    print 'README retrieved'

myFTPObj.quit()

