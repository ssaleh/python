#!/usr/bin/python
# network4.py
# create a TCP Server that will start up, and wait for a client to connect
# in addition this socket uses the setsockopt function which takes level, option, value
# this example makes the socket address reusable so that the port number
# of the socket will be immediately reusable after the socket is closed 

from socket import socket, AF_INET, SOCK_STREAM, SOL_SOCKET, SO_REUSEADDR
from time import ctime
host = ''           # blank means bind() can use any available address
port = 10001
BufferSize = 2048

#Continued on the next page
 
MaxNumInConnections = 3
AddressWhere = (host,port)

MyTCPServer = socket(AF_INET,SOCK_STREAM)
MyTCPServer.setsockopt(SOL_SOCKET,SO_REUSEADDR ,1)
MyTCPServer.bind(AddressWhere)
MyTCPServer.listen(MaxNumInConnections)

while True:
    print 'Server is waiting for Client connection...'
    MyTCPClient, addr = MyTCPServer.accept()
    print '...Client is connected at: ', addr
    MyClientFileObj = MyTCPClient.makefile('rw',0)

    while True:
        data = MyClientFileObj.readline()
        if data == '\n':
            MyClientFileObj.close()
            MyTCPClient.close()
            break    
        print 'Read: ', data
        data = '[%s] %s \n' %(ctime(), len(data))
        MyClientFileObj.write(data)
        print 'Wrote: ', data

MyTCPServer.close()

