# network6.py
# create a UDP Server that will start up, and wait for a client to connect
# all this server does is accept the data sent by a client, and put a timestamp
# in front of the data and return it! 

from socket import socket, AF_INET, SOCK_DGRAM
from time import ctime
host = ''
port = 10001
BufferSize = 4096
AddressWhere = (host,port)

MyUDPServer = socket(AF_INET,SOCK_DGRAM)
MyUDPServer.bind(AddressWhere)

#Continued on the next page
 
while True:
    print 'Server is waiting for message...'
    data, addr = MyUDPServer.recvfrom(BufferSize)
    print 'Message received from %s: %s' % (addr,data)
    data = '[%s] %s' %(ctime(), len(data))
    MyUDPServer.sendto(data,addr)
    print 'Message sent to %s: %s' % (addr,data)
   
MyUDPServer.close()

