#!/usr/bin/python
# network05.py
# create a TCP Client that will start up, and connect to the TCP server
# on the same PC by using localhost
# This one uses a file object to talk to socket

from socket import socket, AF_INET, SOCK_STREAM, gaierror
import sys

host = 'localhost'  # localhost means bind() to same machine as server
port = 10001        # must be SAME in order to talk to server on same PC
BufferSize = 2048   # must be the SAME as server to work
AddressWhere = (host,port)

MyTCPClient = socket(AF_INET,SOCK_STREAM)

try:
    MyTCPClient.connect(AddressWhere)
except gaierror,e:
    print 'Could not connect to TCP Server: %' % e
    sys.exit(1)
 
#Continued on the next page

MyTCPFile = MyTCPClient.makefile('rw',0)
 
while True:
    data = raw_input('Enter data terminated by a blank line: ')
    
    if not data: 
        MyTCPFile.write('\n')
        break
    else:
        data += '\n'

    MyTCPFile.write(data) 
    print 'Wrote: ', data
    data = MyTCPFile.readline()
    print 'Read: ', data

MyTCPFile.close()     
MyTCPClient.close()

