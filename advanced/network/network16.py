#!/usr/bin/python
from socket import *
import sys

host = 'localhost'  # localhost means bind() to same machine as server
port = 10001        # must be SAME in order to talk to server on same PC
BufferSize = 2048   # must be the SAME as server to work
AddressWhere = (host,port)
   
while True:
    MyTCPClient = socket(AF_INET,SOCK_STREAM)
    try:
        MyTCPClient.connect(AddressWhere)
    except gaierror,e:
        print "Could not connect to TCP Server: %s" % e
        sys.exit(1)
        
    data = raw_input('Enter data: ')
    if not data:
        break
    
#Continued on the next page
 
    #need this because server treats as file
    MyTCPClient.send('%s\r\n' %data)
    data = MyTCPClient.recv(BufferSize)
    if not data:
        print "no data obtained" 
        break
    #remove extra \r\n received from server
    print data.strip()              
     
MyTCPClient.close()

