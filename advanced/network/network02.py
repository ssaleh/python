#!/usr/bin/python
# network02.py
# create a TCP Client that will start up, and connect to the TCP server
# on the same PC by using localhost
# Notice the address is now used with connect() function rather than bind()

from socket import socket, AF_INET, SOCK_STREAM, gaierror
import sys

host = 'localhost'  # localhost means bind() to same machine as server
port = 10000        # must be SAME in order to talk to server on same PC
BufferSize = 2048   # must be the SAME as server to work
AddressWhere = (host,port)

MyTCPClient = socket(AF_INET,SOCK_STREAM)

try:
    MyTCPClient.connect(AddressWhere)
except gaierror,e:
    print "Could not connect to TCP Server: %s" % e
    sys.exit(1)
    
#Continued on the next page
 
while True:
    data = raw_input('Enter data: ')

    if not data:
        break
    MyTCPClient.send(data) 
    data = MyTCPClient.recv(BufferSize)

    if not data:
        print "no data obtained" 
        break
    print data
     
MyTCPClient.close()    


