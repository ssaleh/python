# network7.py
# create a UDP Client that will start up, 
# on the same PC by using localhost NO CONNECT
from socket import socket, AF_INET, SOCK_DGRAM
 
host = 'localhost'  # localhost means bind() to same machine as server
port = 10001        # must be SAME in order to talk to server on same PC
BufferSize = 41   # must be the SAME as server to work
AddressWhere = (host,port)

#Continued on the next page
 
MyUDPClient = socket(AF_INET,SOCK_DGRAM)
   
while True:
    data = raw_input('Enter data: ')

    if not data:
        break
    MyUDPClient.sendto(data,AddressWhere) 
    data, AddressWhere = MyUDPClient.recvfrom(BufferSize)

    if not data:
        print "no data obtained" 
        break

    print data
     
MyUDPClient.close()                 

