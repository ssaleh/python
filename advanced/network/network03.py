#!/usr/bin/python
#network3.py
# create a TCP Client that will start up, and connect to the TCP server
# on the Intenet to get the text on a webpage, for example, www.python.org,
# and tell us about the socket and the connection
from socket import socket, AF_INET, SOCK_STREAM
import sys
port = 80
host = 'www.python.org'   
print "Create Internet TCP socket" 
MyTCPClient = socket(AF_INET,SOCK_STREAM)

print "Connect to Internet host computer on port %d: " %port,
MyTCPClient.connect((host,port))

print "Connected from client ", MyTCPClient.getsockname()
print "Connected to server ", MyTCPClient.getpeername()
     
MyTCPClient.close()

