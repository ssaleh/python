#!/usr/bin/python
#network12.py
'''Example module to 'get' HTTP data using httplib'''

import httplib
myHTTPObj = httplib.HTTPConnection("www.python.org")   
myHTTPObj.request("GET", "/index.html")
myHTTPResponse = myHTTPObj.getresponse()
print myHTTPResponse.status, myHTTPResponse.reason
myHTTPData1 = myHTTPResponse.read()

myHTTPObj.request("GET", "/birdbrain.com")
myHTTPResponse = myHTTPObj.getresponse()
print myHTTPResponse.status, myHTTPResponse.reason
myHTTPData = myHTTPResponse.read()
myHTTPObj.close()

