#!/usr/bin/python
def sendMail(host, addr, to, subject, content, password):
    import smtplib
    from email.MIMEText import MIMEText

    print "Sending mail from", addr, "to", to, "...",
    server = smtplib.SMTP(host)
    msg    = MIMEText(content)

    msg["Subject"] = subject
    msg["From"]    = addr
    msg["To"]      = to

#    server.login(addr, password)
#   raised an SMTPException since not supported    
    server.sendmail(addr, [to], msg.as_string())
    server.quit()
    print "done."

to = 'keithw@localhost' #raw_input('who to send the mail to? ')
addr = 'keithw@localhost'
content = 'test message'
subject = 'python test send'
host = 'localhost'
#password = raw_input('what is the password? ').strip()
password = 'not supported'
sendMail(host, addr, to, subject, content, password)

