#!/usr/bin/python
# network1.py
# create a TCP Server that will start up, and wait for a client to connect
# all this server does is accept the data sent by a client, and put a timestamp
# in front of the data and return it! 

#from socket import *
from socket import socket, AF_INET, SOCK_STREAM
from time import ctime
host = ''           # blank means bind() can use any available address
port = 10000
BufferSize = 2048
MaxNumInConnections = 6
AddressWhere = (host,port)

MyTCPServer = socket(AF_INET,SOCK_STREAM)
MyTCPServer.bind(AddressWhere)
MyTCPServer.listen(MaxNumInConnections)

while True:
    print "Server is waiting for Client connection..."
    MyTCPClient, addr = MyTCPServer.accept()
    print "...Client is connected at: ", addr
#Continued on the next page
 
    while True:
        data = MyTCPClient.recv(BufferSize)

        if not data:
            break
        MyTCPClient.send('[%s] %s' %(ctime(), data))

    MyTCPClient.close()

MyTCPServer.close()  

