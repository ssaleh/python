#!/usr/bin/python

import unittest
from test1 import multiply as Mult, add as Add, moreMath as AddMult

class myUnitTest(unittest.TestCase):
   def setUp(self):
        print "set up the test case"

   def testAdd(self):
        for num1 in xrange(-100,100):
            for num2 in xrange(-100,100):
                sum = Add(num1,num2)
                self.failUnless(sum == num1 + num2, "cannot add the two numbers" + str(num1) + " " + str(num2))
  
   def testMultiply(self):
        for num1 in xrange(-100,100):
            for num2 in xrange(-100,100):
                prod = Mult(num1,num2)
                self.failUnless(prod == num1 * num2, "cannot multiply the two numbers"+ str(num1) + " " + str(num2))

   def testMoreMath(self):
        for num1 in xrange(-100,100):
            for num2 in xrange(-100,100):
                result = AddMult(num1,num2)
                self.failIf(result != (num1 + num2) + (num1 * num2), "moreMath did not work for: " + str(num1)+ " " + str(num2))

   def testMoreMath2(self):
        for num1 in xrange(1,10):
            for num2 in xrange(1,10):
                result = AddMult(num1,num2)
                self.failIf(result != (num1 + num2) + (num1 * num2), "moreMath did not work 1 to 10: " + str(num1) + " " + str(num2))

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(myUnitTest))     
    return suite

if __name__ == '__main__':
    #mathSuite = unittest.TestSuite()
    #mathSuite.addTest(myUnitTest("testAdd"))
    #mathSuite.addTest(myUnitTest("testMultiply"))
    #mathSuite.addTest(myUnitTest("testMoreMath"))
    #mathSuite.addTest(myUnitTest("testMoreMath2"))
    unittest.TextTestRunner(verbosity=2).run(suite())
