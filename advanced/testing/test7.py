#!/usr/bin/python

import unittest
from test6 import moreMath as AddMult

class myUnitTest(unittest.TestCase):
    def setUp(self):
        print "set up the test case"

    def testMoreMath(self):
        for num1 in xrange(-100,100):
            for num2 in xrange(-100,100):
                res = AddMult(num1,num2)
                self.failIf(res != (num1 + num2) + (num1 * num2), "moreMath did not work for: " + str(num1)+ " " + str(num2))

    def testMoreMath2(self):
        for num1 in xrange(1,10):
            for num2 in xrange(1,10):
                res = AddMult(num1,num2)
                self.failIf(res != (num1 + num2) + (num1 * num2), "moreMath did not work 1 to 10: " + str(num1) + " " + str(num2))
           
def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(myUnitTest))     
    return suite

if __name__ == '__main__':
    #mathSuite = unittest.TestSuite()
    #mathSuite.addTest(myUnitTest("testMoreMath"))
    #mathSuite.addTest(myUnitTest("testMoreMath2"))
    unittest.TextTestRunner(verbosity=2).run(suite())
