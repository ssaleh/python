class InputError(Exception):
        """Exception raised for negative numbers or zero values.
    
        expression is the erroneous data
        message is what I want to say
        """
    
        def __init__(self, expression, message):
            self.expression = expression
            self.message = message
 
        
