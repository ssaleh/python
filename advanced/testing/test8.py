#doctest1.py
''' Do simple math'''

def add(one,two):
    """
    >>> [add(one,3) for one in range(6)]
    [3, 4, 5, 6, 7, 8]
    >>> [add(one,7) for one in range(-10,10)]
    [-3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
    """   
    return int(one) + int(two)

def subtract(one,two):
    return int(one) - int(two)

def multiply(one,two):
    return int(one) * int(two)
 
def divide(one,two):
    ''' this divides two integers BUT if the second one is zero, you are in trouble'''
    if two != 0:
        return int(one) / int(two)
    else:
        print "you cannot divide by zero in this universe."
        return 0
 
def moreMath(one,two):
    """
    >>> [moreMath(one,6) for one in xrange(-20,20,2)]
    more math adds and multiples to get:  -134
    more math adds and multiples to get:  -120
    more math adds and multiples to get:  -106
    more math adds and multiples to get:  -92
    more math adds and multiples to get:  -78
    more math adds and multiples to get:  -64
    more math adds and multiples to get:  -50
    more math adds and multiples to get:  -36
    more math adds and multiples to get:  -22
    more math adds and multiples to get:  -8
    more math adds and multiples to get:  6
    more math adds and multiples to get:  20
    more math adds and multiples to get:  34
    more math adds and multiples to get:  48
    more math adds and multiples to get:  62
    more math adds and multiples to get:  76
    more math adds and multiples to get:  90
    more math adds and multiples to get:  104
    more math adds and multiples to get:  118
    more math adds and multiples to get:  132
    [-134, -120, -106, -92, -78, -64, -50, -36, -22, -8, 6, 20, 34, 48, 62, 76, 90, 104, 118, 132]
    """
    result = add(one,two) + multiply(one,two)
    print "more math adds and multiples to get: ", result
    return result

if __name__ == "__main__":
    import doctest
    doctest.testmod()
