#myTestClass.py
from myInputErrorClass import InputError
from myDenomErrorClass import DenomError

class myTest:
     "This is a test class with 5 methods and two attributes"
     def __init__(self, num1, num2,aDescription):
          try:
               self.x = int(num1)
          except ValueError:
                print "---------------------------------------------------"
                print "ValueError: The first number is not a valid number."
                print "ValueError: The first number will be set to 1"
                print "---------------------------------------------------"
                self.x = 1
          try:
               self.y = int(num2)
          except ValueError:
                print "---------------------------------------------------"
                print "ValueError: The second number is not a valid number."
                print "ValueError: The second number will be set to 1"
                print "---------------------------------------------------"
                self.y = 1
          self.str = aDescription
          self.checkNegative()
          self.return2Values()
          assert hasattr(self,"num1"), "number 1 is not present"
          assert len(aDescription) > 1
          assert hasattr(self,"num2")
          assert hasattrb(self,"num3")

     def printIt(self):
	  '''I print the attributes of the object, but return nothing'''
          print "---------------------------------------------------"
          print "These are the MyTest object values."
          print "This is the first  number ", self.x
	  print "This is the second number ", self.y
	  print "This is the description ", self.str
	  print "---------------------------------------------------"
	  return

     def return2Values(self):
	  '''I return the two numbers'''
          return "this object has two numbers x and y: ", self.x, self.y

     def divide(self):
          '''I divide x by y, If y is 0 I raise an exception'''
          try:
               return self.x // self.y
          except DenomError:
               print "---------------------------------------------------"
               print ("DenomError: Attempted to divide by zero")
               print " DenomError: second number will be set to 1"
               print "---------------------------------------------------"
               self.y = 1

     def set2Values(self, a, b):
	  '''I set the class values'''
          self.x = a
          self.y = b
          return
     def add(self):
          '''I add the x and y'''
          return self.x + self.y
     def checkNegative(self):
          '''I check whether either number is negative'''
          if self.x < 0:
               raise InputError(self.x, "This class requires the first number to be positive")
          if self.y < 0:
               raise InputError(self.y, "This class requires the second number to be positive")

     def testExceptions(self):
           try:
                self.x = int(num1)
                self.y = int(num2)
                return self.x // self.y
           except ValueError:
                print "---------------------------------------------------"
                print "ValueError: This is not a valid number."
                print "---------------------------------------------------"
           except DenomError:
                print "Cannot divide by zero."
           except:
                print "Unexpected error"
           else:       
               print "Hey! No exceptions occurred!"
           finally:
               print "this is executed whether or not an exception occurred."
               
               
  
