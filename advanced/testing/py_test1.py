#!/usr/bin/python
#py_test.py

from unittest1 import add as Add, subtract as Subt, multiply as Mult, divide as Div, moreMath as AddMult

class myUnitTest():
    
    def test_Add(self):
        for num1 in xrange(-100,100):
            for num2 in xrange(-100,100):
                sum = Add(num1,num2)
                if (sum != num1 + num2):
                    raise ValueError, "Add failed" + str(num1) + " " + str(num2)
          
    def test_Subtract(self):
        for num1 in xrange(-100,100):
            for num2 in xrange(-100,100):
                differ = Subt(num1,num2)
                if (differ != num1 - num2):
                    raise ValueError, "Subtract failed" + str(num1) + " " + str(num2)
 #Continued on the next page
 
    def test_Multiply(self):
        for num1 in xrange(-100,100):
            for num2 in xrange(-100,100):
                prod = Mult(num1,num2)
                if (prod != num1 * num2):
                    raise ValueError, "Multiply failed" + str(num1) + " " + str(num2)
            
    def test_Divide(self):
        for num1 in xrange(-100,100):
            for num2 in xrange(-100,100):               
                quot = Div(num1,num2)
                if (quot != num1 / num2):
                    raise ValueError, "Divide failed" + str(num1) + " " + str(num2)
                
    def testMoreMath(self):
        for num1 in xrange(-100,100):
            for num2 in xrange(-100,100):              
                result = AddMult(num1,num2)
                if (result != int(num1 + num2) + int(num1 * num2)):
                    raise ValueError, "moreMath failed" + str(num1) + " " + str(num2)
