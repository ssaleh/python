class DenomError(Exception):
        """Exception raised for zero denominator division.
    
        expression is the erroneous data
        message is what I want to say
        """
    
        def __init__(self, expression, message):
            self.expression = expression
            self.message = message
