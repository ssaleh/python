#!/usr/bin/python

import unittest

from test1 import add as Add, subtract as Subt, multiply as Mult, divide as Div, moreMath as AddMult

class myUnitTest(unittest.TestCase):

    def setUp(self):
        print "if you need to set up anything for all tests do it here"

    def testAdd(self):
        for num1 in xrange(-100,100):
            for num2 in xrange(-100,100):
                sum = Add(num1,num2)
                self.failUnless(sum == num1 + num2, "cannot add the two numbers")

    def testSubtract(self):
        for num1 in xrange(-100,100):
            for num2 in xrange(-100,100):
                differ = Subt(num1,num2)
                self.failUnless(differ == num1 - num2, "cannot subtract the two numbers")

    def testMultiply(self):
        for num1 in xrange(-100,100):
            for num2 in xrange(-100,100):
                prod = Mult(num1,num2)
                self.failUnless(prod == num1 * num2, "cannot multiply the two numbers")

    def testDivide(self):
	        for num1 in xrange(-100,100):
	            for num2 in xrange(-100,100):               
	                quot = Div(num1,num2)
	                self.failUnless(quot == num1 / num2, "cannot divide the two numbers")

    def testMoreMath(self):
	        for num1 in xrange(-100,100):
	            for num2 in xrange(-100,100):              
	                result = AddMult(num1,num2)
	                self.failIf(result != (num1 + num2) + (num1 * num2), "cannot do moreMath on these two numbers")

def suite():
	    suite = unittest.TestSuite()
	    suite.addTest(unittest.makeSuite(myUnitTest))     
	    return suite

if __name__ == '__main__':
      unittest.TextTestRunner(verbosity=2).run(suite())    
