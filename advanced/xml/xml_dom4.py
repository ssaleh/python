#!/usr/bin/python
# xml_dom4.py

from xml.dom.minidom import *
def createNewDoc():
    impl = getDOMImplementation()
    aDocument = impl.createDocument(None, None, None)
    BusinessNode = aDocument.createElement("business")
    aDocument.appendChild(BusinessNode)
    descriptionTextNode = aDocument.createTextNode("This is all about DiamondCents Business Locations")
    BusinessNode.appendChild(descriptionTextNode)
    return aDocument, BusinessNode

def createNewLocation(doc, business, desc):
    LocationNode = doc.createElement("location")
    business.appendChild(LocationNode)
    descriptionTextNode = doc.createTextNode(desc)
    LocationNode.appendChild(descriptionTextNode)
    return LocationNode
 
def createNewSurroundings(doc, location, desc):
    SurroundingNode = doc.createElement("surrounding")
    location.appendChild(SurroundingNode)
    descriptionTextNode = doc.createTextNode(desc)
    SurroundingNode.appendChild(descriptionTextNode)
    return SurroundingNode
    
def createNewLocationName(doc, location, desc):
    LocationNameNode = doc.createElement("name")
    location.appendChild(LocationNameNode)
    descriptionTextNode = doc.createTextNode(desc)
    LocationNameNode.appendChild(descriptionTextNode)
    return LocationNameNode

def getText(nodelist):
    rc = ""
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc = rc + node.data
    return rc
 
def processBusiness(business):
    print "<html>"
    processBusinessTitle(business.getElementsByTagName("business")[0])
    locations = business.getElementsByTagName("location")
    processBusinessLocationNames(locations)
    processBusinessLocations(locations)
    print "</html>"

def processBusinessTitle(business):
    print "<title>%s</title>" % getText(business.childNodes)
    

def processBusinessLocationNames(locations):
    print "<ul>"
    for location in locations:
        name = location.getElementsByTagName("name")[0]
        print "<li>%s</li>" % getText(name.childNodes)
    print "</ul>"

 
def processBusinessLocations(locations):
    for location in locations:
        processLocationName(location.getElementsByTagName("name")[0])
        processLocationSurrounding(location.getElementsByTagName("surrounding")[0])

def processLocationName(location):
    print "<h2>%s</h2>" % getText(location.childNodes)
def processLocationSurrounding(location):
    print "<p>Price: %s</p>" % getText(location.childNodes)
       
aTestDoc, aBusiness = createNewDoc()
aNewLocation1 = createNewLocation(aTestDoc, aBusiness, "345 Everywhere St, San Diego, 92121")
aLocalName1 = createNewLocationName(aTestDoc, aNewLocation1, "Peony Store")
allAroundIt1 = createNewSurroundings(aTestDoc, aNewLocation1, "peonies in front, sunny, urban")
aNewLocation2 = createNewLocation(aTestDoc, aBusiness, "56 3/4 Somewhere else St, San Diego, 92020")
aLocalName2 = createNewLocationName(aTestDoc, aNewLocation2, "Grassy Store")
allAroundIt2 = createNewSurroundings(aTestDoc, aNewLocation2, "grassy fields all around, flowers, sunny")
aNewLocation3 = createNewLocation(aTestDoc, aBusiness, "99  Sunset St, San Diego, 92126")
aLocalName3 = createNewLocationName(aTestDoc, aNewLocation3, "Brick Store")
allAroundIt3 = createNewSurroundings(aTestDoc, aNewLocation3, "tall brick building, art deco, green awnings")

processBusiness(aTestDoc)
