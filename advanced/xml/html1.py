#!/usr/bin/python
#html1.py
# this class is a type of HTML Parser that only looks for <ahef  ...> tags
from HTMLParser import HTMLParser

class AnchorParser(HTMLParser):

    def __init__(self):
        self.ListOfAnchors = []
        self.reset()

    def handle_starttag(self, tag, elementAttributes):
        if tag == "a":
            #print elementAttributes
            for i, j in elementAttributes:
                if i == "href":
                    self.ListOfAnchors.append(j)
                    break
 
anHTMLDocument = open("HTMLParser.html")

aParser = AnchorParser()
aParser.feed(anHTMLDocument.read())
aParser.close()

print aParser.ListOfAnchors
