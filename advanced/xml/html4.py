#!/usr/bin/python
#html4.py
# this class is a type of SGML Parser that only looks for <ahef  ...> tags
# but now you know where things came from
import urllib, sgmllib

class AnchorParser(sgmllib.SGMLParser):
    "an example."

 
    def __init__(self, verbose=0):   
        "must explicitly call your parent class init method to invoke it in Python"
        "the __init__ method is NOT a constructor"
        sgmllib.SGMLParser.__init__(self, verbose)
        self.hyperlinks = []
        self.descriptions = []
        self.inside_a_element = 0
        
    def parse(self, s):
        "parse whatever you are given once this is called all start_tag and end_tag methods will be called"
        
        self.feed(s)
        self.close()
        
    def start_a(self, attributes):
        "Process href plus its attributes"
        for name, value in attributes:
            if name == "href":
                self.hyperlinks.append(value)
                self.inside_a_element = 1
    def end_a(self):
        "reset flag at the the end of the href."
        self.inside_a_element = 0
 
    def get_hyperlinks(self):
        "Return the list of hyperlinks."
        return self.hyperlinks

    def handle_data(self, data):
        "Handle the 'data'."
        if self.inside_a_element:
            self.descriptions.append(data)

    def get_descriptions(self):
        "Return a list of hyperlink descriptions."
        return self.descriptions

aDocument = urllib.urlopen("http://www.python.org")
stuff = aDocument.read()
aParser = AnchorParser()
aParser.parse(stuff)

# print aParser.get_hyperlinks()
print aParser.get_descriptions()
