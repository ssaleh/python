#!/usr/bin/python
#html2.py
# this class is a type of HTML Parser that only looks for <ahef  ...> tags
from HTMLParser import HTMLParser

class AnchorParser(HTMLParser):

    def __init__(self):
        self.ListOfAnchors = []
        self.reset()

    def handle_starttag(self, tag, elementAttributes):
        if tag == "a":
            for i, j in elementAttributes:
                if i == "href":
                    self.ListOfAnchors.append(j)
                    break
 
def getanchors(file):
    aParser = AnchorParser()
    while 1:
        # get some data from the source HTML Document
        anHTMLDocument = file.read(16384) #bytes to read
        if anHTMLDocument:
            aParser.feed(anHTMLDocument)
        else:
            aParser.close()
        # return anchors to caller
        print len(aParser.ListOfAnchors)
        for anchor in aParser.ListOfAnchors:
            yield anchor
        if not anHTMLDocument:
            break
        aParser.ListOfAnchors[:] = []  

# read from a remote site
from urllib import urlopen
for anchor in getanchors(urlopen("http://www.python.org")):
    print anchor
