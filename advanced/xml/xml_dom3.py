#!/usr/bin/python
#xml_dom3.py
from xml.dom.minidom import *
XMLDocument = '''
<breakfast>
    <name>Breakfast of Champions</name>
    <food>
        <name>Belgian Waffles</name>
        <price>$5.95</price>
        <description>
        two of our famous Belgian Waffles with plenty of real maple syrup
        </description>
        <calories>650</calories>
    </food>
    <food>
        <name>Strawberry Belgian Waffles</name>
        <price>$7.95</price>
        <description>
        light Belgian waffles covered with strawberries and whipped cream
        </description>
 
        <calories>900</calories>
    </food>
    <food>
        <name>Berry-Berry Belgian Waffles</name>
        <price>$8.95</price>
        <description>
        light Belgian waffles covered with an assortment of fresh berries and whipped cream
        </description>
        <calories>900</calories>
    </food>
    <food>
        <name>French Toast</name>
        <price>$4.50</price>
        <description>
        thick slices made from our homemade sourdough bread
        </description>
        <calories>600</calories>
    </food>
    <food>
        <name>Homestyle Breakfast</name>
        <price>$6.95</price>
        <description>
 
        two eggs, bacon or sausage, toast, and our ever-popular hash browns
        </description>
        <calories>950</calories>
    </food>
</breakfast>
'''

def getText(nodelist):
    rc = ""
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc = rc + node.data
    return rc

def processBreakfast(breakfast):
    print "<html>"
    processBreakfastName(breakfast.getElementsByTagName("name")[0])
    foods = breakfast.getElementsByTagName("food")
    processBreakfastMenu(foods)
    processFoods(foods)
    print "</html>"
 
def processBreakfastName(name):
    print "<title>%s</title>" % getText(name.childNodes)
    
def processBreakfastMenu(foods):
    print "<ul>"
    for food in foods:
        name = food.getElementsByTagName("name")[0]
        print "<li>%s</li>" % getText(name.childNodes)
    print "</ul>"
    
def processFoods(foods):
    for food in foods:
        processFoodName(food.getElementsByTagName("name")[0])
        processFoodPrice(food.getElementsByTagName("price")[0])
        processFoodDesc(food.getElementsByTagName("description")[0])
        processFoodCalories(food.getElementsByTagName("calories")[0])
        
def processFoodName(food):
    print "<h2>%s</h2>" % getText(food.childNodes)
def processFoodPrice(food):
    print "<p>Price: %s</p>" % getText(food.childNodes)
def processFoodDesc(food):
    print "<p>Description: %s</p>" % getText(food.childNodes)
 
def processFoodCalories(food):
    print "<p>Calories: %s</p>" % getText(food.childNodes)    
    
dom = parseString(XMLDocument) # parseString is a function imported from xml.dom.minidom
processBreakfast(dom)          # it returns a Document object that represents the string
