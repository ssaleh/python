#!/usr/bin/python
#html6.py
import urllib, htmllib, formatter

class LinksExtractor(htmllib.HTMLParser):  

   def __init__(self, formatter) :         
      htmllib.HTMLParser.__init__(self, formatter)   
      self.hyperlinks= []
      
   def start_a(self, attrs) :   
      # process the attributes
      if len(attrs) > 0 :
         for attr in attrs: 
            if attr[0] == "href" :          
                self.hyperlinks.append(attr[1])  

   def get_hyperlinks(self):      
        return self.hyperlinks
 
aFormatter = formatter.NullFormatter()            
aParser = LinksExtractor(aFormatter)         
data = urllib.urlopen("http://www.python.org")
aParser.feed(data.read())      
aParser.close()

hyperlinks= aParser.get_hyperlinks()    
print hyperlinks   
