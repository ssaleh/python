#!/usr/bin/python
#xml_sax1.py

import sys
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
 
class BreakfastHandler(ContentHandler):
    def startElement(self, name, attrs):
        print "Entering element ",name
 
eatIt = BreakfastHandler()
saxparser = make_parser()
saxparser.setContentHandler(eatIt)
 
datasource = open("breakfast.xml","r")
saxparser.parse(datasource)
