#!/usr/bin/python
#xml_dom2.py

from xml.dom.minidom import *
def createNewDoc():
    impl = getDOMImplementation()
    aDocument = impl.createDocument(None, None, None)    
    BusinessNode = aDocument.createElement("business")
    aDocument.appendChild(BusinessNode)
    return aDocument, BusinessNode

def createNewLocation(doc, business, desc):
    LocationNode = doc.createElement("location")
    business.appendChild(LocationNode)
    descriptionTextNode = doc.createTextNode(desc)
    LocationNode.appendChild(descriptionTextNode)
    return LocationNode
 
def createNewSurroundings(doc, location, desc):
    SurroundingNode = doc.createElement("surrounding")
    location.appendChild(SurroundingNode)
    descriptionTextNode = doc.createTextNode(desc)
    SurroundingNode.appendChild(descriptionTextNode)
    return SurroundingNode
     
aTestDoc, aBusiness = createNewDoc()
aNewLocation1 = createNewLocation(aTestDoc, aBusiness, "345 Everywhere St, San Diego, 92121")
allAroundIt1 = createNewSurroundings(aTestDoc, aNewLocation1, "peonies in front, sunny, urban")
descriptionTextNode = aTestDoc.createTextNode("Before or after child element node?")
aNewLocation1.appendChild(descriptionTextNode)
aNewLocation2 = createNewLocation(aTestDoc, aBusiness, "56 3/4 Somewhere else St, San Diego, 92020")
allAroundIt2 = createNewSurroundings(aTestDoc, aNewLocation2, "grassy fields all around, flowers, sunny")
aNewLocation3 = createNewLocation(aTestDoc, aBusiness, "99  Sunset St, San Diego, 92126")
allAroundIt3 = createNewSurroundings(aTestDoc, aNewLocation3, "tall brick building, art deco, green awnings")
 
print aTestDoc
print aBusiness
print aNewLocation1
print aNewLocation2
print aNewLocation3
print allAroundIt1
print allAroundIt2
print allAroundIt3
print aTestDoc.toprettyxml()
file = open("business.xml", 'w')
file.write(aTestDoc.toprettyxml())
file.close()
