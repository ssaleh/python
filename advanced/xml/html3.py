#!/usr/bin/python
#html3.py
# this class is a type of SGML Parser that only looks for <ahef  ...> tags
# but its output is difficult to read
import urllib, sgmllib

class AnchorParser(sgmllib.SGMLParser):
    "an example."

    def __init__(self, verbose=0):   
        "must explicitly call your parent class init method to invoke it in Python"
        "the __init__ method is NOT a constructor"
        sgmllib.SGMLParser.__init__(self, verbose)
        self.hyperlinks = []
        
    def parse(self, s):
        "parse whatever you are given"
        self.feed(s)
        self.close()
 
    def start_a(self, attributes):
        "Process href plus its attributes"
        for name, value in attributes:
            if name == "href":
                self.hyperlinks.append(value)

    def get_hyperlinks(self):
        "Return the list of hyperlinks."
        return self.hyperlinks

aDocument = urllib.urlopen("http://www.python.org")
stuff = aDocument.read()
aParser = AnchorParser()
aParser.parse(stuff)
for anchor in aParser.get_hyperlinks():
    print anchor
