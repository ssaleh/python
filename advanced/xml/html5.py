#!/usr/bin/python
#html5.py
import urllib
import htmllib
import formatter
aFormatter = formatter.NullFormatter()       # create default formatter *
aParser = htmllib.HTMLParser(aFormatter)  # create new parser object

data = urllib.urlopen("http://www.python.org")
aParser.feed(data.read())
aParser.close()
