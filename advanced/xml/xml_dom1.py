#!/usr/bin/python
#xml_dom1.py

from xml.dom.minidom import *
def createNewDoc():
    aDocument = Document()
    BusinessNode = aDocument.createElement("business")
    aDocument.appendChild(BusinessNode)
    return aDocument, BusinessNode

def createNewLocation(doc, business):
    LocationNode = doc.createElement("location")
    business.appendChild(LocationNode)
    return LocationNode
    
aTestDoc, aBusiness = createNewDoc()
aNewLocation = createNewLocation(aTestDoc, aBusiness)
print aTestDoc
print aBusiness
print aNewLocation
#aTestDoc.print()
