#!/usr/bin/python
#profile4.py
''' This module is for testing performance using profile module'''

import profile
import pstats

def add(one,two):
    ''' this adds two integers'''
    return int(one) + int(two)

def multiply(one,two):
    return int(one) * int(two)
 
def divide(one,two):
    ''' this divides two integers BUT if the second one is zero, you are in trouble'''
    if two != 0:
        return int(one) / int(two)
    else:
        print "you cannot divide by zero in this universe."
        return 0

def third_power(one,two):
	return one ** two ** two

def moreMath(one,two):
    print "more math", add(one,two) + multiply(one,two) + third_power(5, 2)
    return


print 'test1:', add(5,6)
print 'test2:',multiply(40,30)

if __name__ == "__main__":
    print '1:' , add(3,4) 
    print '2:', add(0,5) 
    print '3:' , divide(3,4) 
    print '4:', divide(-3,4) 
    p = profile.Profile()
    p.run("moreMath(321233456,456)")
    s = pstats.Stats(p)
    s.strip_dirs()
    s.sort_stats("time").print_stats()
