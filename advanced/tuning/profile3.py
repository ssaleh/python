#!/usr/bin/python
#profile3.py
''' This module is for testing performance using profile module'''

import profile
import pstats

def add(one,two):
    ''' this adds two integers'''
    return int(one) + int(two)

def multiply(one,two):
    return int(one) * int(two)
 
def divide(one,two):
    ''' this divides two integers BUT if the second one is zero, you are in trouble'''
    if two != 0:
        return int(one) / int(two)
    else:
        print "you cannot divide by zero in this universe."
        return 0

def moreMath(one,two):
    print "more math", add(one,two) + multiply(one,two) + add(one,two) + add(one,two)
    return

print 'test1:', add(5,6)
print 'test2:',multiply(40,30)

if __name__ == "__main__":
    print '1:' , add(3,4) 
    print '2:', add(0,5) 
    print '3:' , divide(3,4) 
    print '4:', divide(-3,4) 
    profile.run("moreMath(32,456)")
