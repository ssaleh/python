#!/usr/bin/python
#profile2.py

import profile
import pstats

def add(one,two):
    return int(one) + int(two)

def multiply(one,two):
    return int(one) * int(two)

def divide(one,two):
    if two != 0:
        return int(one) / int(two)
    else:
        print "you cannot divide by zero in this universe."
        return 0
 
def moreMath(one,two):
    print "more math", add(one,two) + multiply(one,two)
    return

print 'test1:', add(5,6)
print 'test2:',divide(90,30)

if __name__ == "__main__":
    profile.run("add(5,6)")
    profile.run("divide(90,30)")
