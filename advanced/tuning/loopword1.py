#!/usr/bin/python
# slow string concatenation
# versus faster join
# open the words file
# concatenate all the words
import profile
import pstats

def loopwords():
    wordsfile = open('words')
    uword= []
    for word in wordsfile:
        uword.append(word.upper())
    wordsfile.close()
    print 'uword list is %s elements long using for loop append' % (len(uword))

def loopnodot():
    wordsfile = open('words')
    uword= []
    append = uword.append
    for word in wordsfile:
        append(word.upper())
    wordsfile.close()
    print 'uword list is %s elements long using for loop with no dot append' % (len(uword))
    
def mapwords():
    wordsfile = open('words')
    uword = map(str.upper, wordsfile)
    wordsfile.close()
    print 'uword list is %s elements long using map' % (len(uword))

if __name__ == "__main__":
    profile.run("loopwords()")
    profile.run("mapwords()")
    profile.run("loopnodot()")
