#!/usr/bin/python
# range vs. xrange
#import sys
from timeit import Timer
BIGNUM = 10000000 # must be less than sys.maxint
def ranger():
    bigrange = range(BIGNUM)
    print 'The bigrange list now has %s elements created with the range function' % (len(bigrange))
    
def xranger():
    bigrange = xrange(BIGNUM)
    print 'The bigrange list now has %s elements created with the xrange function' % (len(bigrange))

if __name__ == '__main__':
    t = Timer("ranger()", "from __main__ import ranger")
    print t.timeit(1)
    t = Timer("xranger()", "from __main__ import xranger")
    print t.timeit(1)        
