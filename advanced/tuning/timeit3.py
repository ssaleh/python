#!/usr/bin/python
#timeit3.py
from timeit import Timer
 
def add(one,two):
    return int(one) + int(two)

def multiply(one,two):
    return int(one) * int(two)
 
def moreMath(one,two):
    print "more math", add(one,two) + multiply(one,two)
    return

if __name__ == "__main__":
    t = Timer("moreMath(1,2)", "from __main__ import moreMath")
    print t.repeat(3,1)
