#!/usr/bin/python

from timeit import Timer


def add(one,two):
    return int(one) + int(two)

def multiply(one,two):
    return int(one) * int(two)

def moreMath(one,two):
    return add(one,two) + multiply(one,two)
    

if __name__ == "__main__":
    t = Timer("moreMath(1,2)", "from __main__ import moreMath")
    #print t.repeat(3,5)

    print t.timeit()        #one time through
    print t.timeit(100)        #one hundred times through
    #print t.repeat(1,2)
