#!/usr/bin/python
#hotshot1.py
import hotshot, hotshot.stats

def add(one,two):
    return int(one) + int(two)

def multiply(one,two):
    return int(one) * int(two)

def moreMath(one,two):
    print "more math", add(one,two) + multiply(one,two)
    return

print 'test1:', add(5,6)
print 'test2:',multiply(40,30)

if __name__ == "__main__":
    p = hotshot.Profile("hotshot_profile_moreMath.txt") #file for output
    p.run("moreMath(32,456)")
    p.close()
    #create pstats object
    s = hotshot.stats.load("hotshot_profile_moreMath.txt")
    s.sort_stats("time").strip_dirs().print_stats()
