#!/usr/bin/python
# slow string concatenation
# versus faster join
# open the words file
# concatenate all the words
from timeit import Timer

def concatwords():
    wordsfile = open('words')
    bigword=''
    for word in wordsfile:
        bigword += word
    wordsfile.close()
    print 'bigword is %s characters long using concatwords' % (len(bigword))

def joinwords():
    wordsfile = open('words')
    bigword = ''.join(wordsfile)
    wordsfile.close()
    print 'bigword is %s characters long using joinwords' % (len(bigword))

    
if __name__ == "__main__":
    t1 = Timer("concatwords()", "from __main__ import concatwords")
    print t1.timeit(3)
    print t1.repeat(3,1)
    t2 = Timer("joinwords()", "from __main__ import joinwords")
    print t2.repeat(3,1)

