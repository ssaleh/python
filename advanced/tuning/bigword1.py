#!/usr/bin/python
# slow string concatenation
# versus faster join
# open the words file
# concatenate all the words
import profile
import pstats

def concatwords():
    wordsfile = open('words')
    bigword=''
    for word in wordsfile:
        bigword += word
    wordsfile.close()
    print 'bigword is %s characters long using concatwords' % (len(bigword))


def joinwords():
    wordsfile = open('words')
    bigword = ''.join(wordsfile)
    wordsfile.close()
    print 'bigword is %s characters long using joinwords' % (len(bigword))

def profileadv():
    p = profile.Profile()
    p.run("joinwords()")
    s = pstats.Stats(p)
    print 'Advanced options of profile output:'
    s.sort_stats("time", "name").print_stats()
    
if __name__ == "__main__":
    profile.run("concatwords()")
    profile.run("joinwords()")
    profileadv()
