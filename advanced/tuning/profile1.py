#!/usr/bin/python
#profile1.py

def add(one,two):
    return int(one) + int(two)

def multiply(one,two):
    return int(one) * int(two)

def divide(one,two):
    ''' this divides two integers BUT if the second one is zero, you are in trouble'''
    if two != 0:
        return int(one) / int(two)
    else:
        print "you cannot divide by zero in this universe."
        return 0
 
def moreMath(one,two):
    print "more math", add(one,two) + multiply(one,two)
    return

print 'test1:', add(5,6)
print 'test2:',divide(90,30)
