#!/usr/bin/python
# slow string concatenation
# versus faster join
# open the words file
# concatenate all the words
import hotshot
import hotshot.stats

def concatwords():
    wordsfile = open('words')
    bigword=''
    for word in wordsfile:
        bigword += word
    wordsfile.close()
    print 'bigword is %s characters long using concatwords' % (len(bigword))


def joinwords():
    wordsfile = open('words')
    bigword = ''.join(wordsfile)
    wordsfile.close()
    print 'bigword is %s characters long using joinwords' % (len(bigword))

    
if __name__ == "__main__":
    p = hotshot.Profile("bigwords2_hotshot_profile.txt") #file for output
    p.run("concatwords()")
    p.run("joinwords()")
    p.close()
    #create pstats object
    s = hotshot.stats.load("bigwords2_hotshot_profile.txt")
    s.sort_stats("time").print_stats()

