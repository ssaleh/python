#!/usr/bin/python
# gui09.py
import wx
theApp = wx.App()

theMainFrame = wx.Frame(None, title='First Program', size=(550,455)) 
theSaveButton = wx.Button(theMainFrame, label='Save', pos=(250,5),size=(80, 25))
theQuitButton = wx.Button(theMainFrame, style = wx.ID_CLOSE,label='Quit', pos=(350,5),size=(80, 25)) 
theOpenButton = wx.Button(theMainFrame, label='Open', pos=(450,5),size=(80, 25))

theTextFile = wx.TextCtrl(theMainFrame,pos=(5,5), size=(225,25))
theTextArea = wx.TextCtrl(theMainFrame,pos=(5,35), size=(530,375), style=wx.TE_MULTILINE | wx.HSCROLL)

theMainFrame.Show()
theApp.MainLoop()

