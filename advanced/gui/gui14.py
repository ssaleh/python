#gui14.py
# jython example
from javax.swing import *
import sys

def testClickButton(event):
    print "You clicked the button"
def testColorButton(event):
    print "Do you like color?"

def closeWindows(event):
    sys.exit()
theMainFrame = JFrame("Jython Example")
 
theClickButton=JButton("Click me!")
theClickButton.actionPerformed=testClickButton

theColorButton=JButton("Colors!")
theColorButton.actionPerformed=testColorButton

theQuitButton=JButton("Quit")                     
theQuitButton.actionPerformed=closeWindows

theMainFrame.contentPane.add(theClickButton)
theMainFrame.contentPane.add(theColorButton)
theMainFrame.windowClosing = closeWindows
theMainFrame.show()

