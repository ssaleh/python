#!/usr/bin/python
#gui11.py
import wx
import sys

def openFile(event):
    try:
        fn = open(theTextFile.GetValue(),'r')
        theTextArea.SetValue(fn.read())
        fn.close()
    except:
        theTextFile.SetValue("I/O error has occured")
        theTextArea.SetValue("Please try a different file name")

def saveFile(event):
    try:
        fn = open(theTextFile.GetValue(),'w')
        fn.write(theTextArea.GetValue())
        fn.close()
    except:
        theTextArea.SetValue("Cannot save the file")
        #Continued on the next page:
 
def quitApp(event):
    sys.exit()
       
theApp = wx.App()
theMainFrame = wx.Frame(None, title='First Program', size=(550,455))

# place components into a large container; order not important
holderPanel = wx.Panel(theMainFrame)
theSaveButton = wx.Button(holderPanel, label='Save', pos=(250,5),size=(80, 25))
theSaveButton.Bind(wx.EVT_BUTTON,saveFile)

theQuitButton = wx.Button(holderPanel, style = wx.ID_CLOSE,label='Quit', pos=(350,5),size=(80, 25)) 
theQuitButton.Bind(wx.EVT_BUTTON,quitApp)

theOpenButton = wx.Button(holderPanel, label='Open', pos=(450,5),size=(80, 25))
theOpenButton.Bind(wx.EVT_BUTTON,openFile)

theTextFile = wx.TextCtrl(holderPanel,pos=(5,5), size=(225,25))
theTextArea = wx.TextCtrl(holderPanel,pos=(5,35), size=(530,375), style=wx.TE_MULTILINE | wx.HSCROLL)
#Continued on the next page
 
#make the sizers and place the components inside them in order
horizonSizer = wx.BoxSizer()  
horizonSizer.Add(theTextFile,proportion=1, flag=wx.EXPAND)
horizonSizer.Add(theSaveButton,proportion=0, flag=wx.LEFT, border=5)
horizonSizer.Add(theQuitButton,proportion=0, flag=wx.LEFT, border=5)
horizonSizer.Add(theOpenButton,proportion=0, flag=wx.LEFT, border=5)

#this sizer will hold the above one plus the larger text area
vertSizer=wx.BoxSizer(wx.VERTICAL)
vertSizer.Add(horizonSizer,proportion=0, flag=wx.EXPAND | wx.ALL, border=5)
vertSizer.Add(theTextArea,proportion=1, flag=wx.EXPAND | wx.LEFT, border=5)

holderPanel.SetSizer(vertSizer)
theMainFrame.Show()
theApp.MainLoop()

