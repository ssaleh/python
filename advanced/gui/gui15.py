''' Simple demo of Jython and Swing
    by Kent Johnson '''

from __future__ import nested_scopes

import sys
from java.awt import GridLayout
from javax.swing import JButton, JFrame, JLabel, JPanel

increments = [1, 10, -1, -10]

class TestFrame(JFrame):
    def __init__(self, title, **kwd):
        JFrame.__init__(self, title, **kwd)

        self.contentPane.setLayout(GridLayout(1, 2))
        
        # Make a button panel with Add buttons for each increment and a Reset button
        buttonPanel = JPanel(GridLayout(0, 1))
        
        for inc in increments:
            button = JButton('Add ' + str(inc))
            button.actionPerformed = lambda evt, inc=inc: self.bumpBy(inc)
            buttonPanel.add(button)
        
        resetButton = JButton('Reset', actionPerformed = self.reset)
        buttonPanel.add(resetButton)
        
        self.contentPane.add(buttonPanel)
        
        # Make a counter centered in the right side
        self.counter = JLabel('0', JLabel.CENTER)
        self.contentPane.add(self.counter)

    def bumpBy(self, inc):
        ''' Increment the counter by inc '''
        value = self.counter.getText()
        value = int(value) + inc
        self.counter.setText(str(value))


    def reset(self, evt):
        self.counter.setText('0')
        
if __name__ == '__main__':
    f = TestFrame('Test Window', windowClosing=lambda event: sys.exit(0))
    f.pack()
    f.setVisible(1)

