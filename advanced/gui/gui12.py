#!/usr/bin/python
#gui11.py
import wx
import sys
import os

class MainStatusBar(wx.StatusBar):
    def __init__(self, parent):
        wx.StatusBar.__init__(self, parent, -1)

        # This status bar has three fields
        self.SetFieldsCount(1)
        # Sets the three fields to be relative widths to each other.
        #self.SetStatusWidths([-2, -1, -2])
        #self.log = log
        #self.sizeChanged = False
        #self.Bind(wx.EVT_SIZE, self.OnSize)
        #self.Bind(wx.EVT_IDLE, self.OnIdle)

        # Field 0 ... just text
        self.SetStatusText("First Application Ready...", 0)

class MainFrame(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, title='First Program', size=(550,455))

        # add a status bar
        self.sb = MainStatusBar(self)
        self.SetStatusBar(self.sb)
        # place components into a large container; order not important
        holderPanel = wx.Panel(self)
        theSaveButton = wx.Button(holderPanel, label='Save', pos=(350,5),size=(80, 25))
        theSaveButton.Bind(wx.EVT_BUTTON,self.saveFile)

        theQuitButton = wx.Button(holderPanel, style = wx.ID_CLOSE,label='Quit', pos=(450,5),size=(80, 25)) 
        theQuitButton.Bind(wx.EVT_BUTTON,self.quitApp)

        theOpenButton = wx.Button(holderPanel, label='Open', pos=(250,5),size=(80, 25))
        theOpenButton.Bind(wx.EVT_BUTTON,self.openFile)

        self.theTextFile = wx.TextCtrl(holderPanel,pos=(5,5), size=(225,25))
        self.theTextArea = wx.TextCtrl(holderPanel,pos=(5,35), size=(530,375), style=wx.TE_MULTILINE | wx.HSCROLL)
         
        #make the sizers and place the components inside them in order
        horizonSizer = wx.BoxSizer()  
        horizonSizer.Add(self.theTextFile,proportion=1, flag=wx.EXPAND)
        horizonSizer.Add(theOpenButton,proportion=0, flag=wx.LEFT, border=5)
        horizonSizer.Add(theSaveButton,proportion=0, flag=wx.LEFT, border=5)
        horizonSizer.Add(theQuitButton,proportion=0, flag=wx.LEFT, border=5)

        #this sizer will hold the above one plus the larger text area
        vertSizer=wx.BoxSizer(wx.VERTICAL)
        vertSizer.Add(horizonSizer,proportion=0, flag=wx.EXPAND | wx.ALL, border=5)
        vertSizer.Add(self.theTextArea,proportion=1, flag=wx.EXPAND | wx.LEFT, border=5)

        holderPanel.SetSizer(vertSizer)
        
        # prepare the open dialog

    def openFile(self, event):
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard='*.*',
            style=wx.OPEN |  wx.CHANGE_DIR | wx.FILE_MUST_EXIST
            )
        if dlg.ShowModal() == wx.ID_OK:
        # This returns a Python list of files that were selected.
            path = dlg.GetPath()
            #print path
        else:
            path = None
        if path:
            self.theTextFile.SetValue(path)
            try:
                fn = open(self.theTextFile.GetValue(),'r')
                self.theTextArea.SetValue(fn.read())
                fn.close()
                self.sb.SetStatusText("File opened successfully...", 0)
            except:
                self.sb.SetStatusText("Error opening file...", 0)
                #self.theTextFile.SetValue("I/O error has occured")
                #self.theTextArea.SetValue("Please try a different file name")

    def saveFile(self, event):
        dlg = wx.FileDialog(
            self, message="Save file as ...", defaultDir=os.getcwd(), 
            defaultFile=self.theTextFile.GetValue(), wildcard='*.*',
            style=wx.SAVE
            )
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            print path
            dlg.Destroy()
            if path:
                self.theTextFile.SetValue(path)
                try:
                    fn = open(self.theTextFile.GetValue(),'w')
                    fn.write(self.theTextArea.GetValue())
                    fn.close()
                    self.sb.SetStatusText("File saved successfully...", 0)
                except:
        #            self.theTextArea.SetValue("Cannot save the file")
                    self.sb.SetStatusText("Error saving file...", 0)

     
    def quitApp(self, event):
        dlg = wx.MessageDialog(self, 'Do you want to quit?',
                               'Question',
                               wx.YES_NO
                               # wx.ICON_INFORMATION | wx.OK
                               #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                               )
        answer = dlg.ShowModal()
        YES, NO = 5103, 5104
        #print answer
        dlg.Destroy()
        if answer == YES:
            sys.exit()

       
theApp = wx.App()
theMainFrame = MainFrame(None)
theMainFrame.Show()
theApp.MainLoop()

