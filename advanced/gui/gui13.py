#!/usr/bin/python
from Tkinter import *
import sys

def testClickButton():
    print "You clicked the button"
def testColorButton():
    print "Do you like color?"
def testQuitButton():
    print "this really won't quit"
    sys.exit()

theMainFrame = Tk()
theMainFrame.title("tkinter example")
theMainFrame.geometry('200x100')

theClickButton=Button(theMainFrame,text="Click me!", command=testClickButton)
theColorButton=Button(theMainFrame,text="Colors!",fg="yellow",bg="blue", 
command=testColorButton)
 
theQuitButton=Button(theMainFrame,text="Quit", fg="red",command=testQuitButton)                     
theClickButton.pack(expand=YES,side=LEFT)

theColorButton.pack()
theQuitButton.pack()

mainloop()

