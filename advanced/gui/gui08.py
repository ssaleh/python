#!/usr/bin/python
#gui08.py
import wx
theApp = wx.App()

theMainFrame = wx.Frame(None, title='First Program', size=(550,335)) 

theSaveButton = wx.Button(theMainFrame, label='Save', pos=(250,5),size=(80, 25))
theQuitButton = wx.Button(theMainFrame, label='Quit', pos=(350,5),size=(80, 25)) 
theOpenButton = wx.Button(theMainFrame, label='Open', pos=(450,5),size=(80, 25))

theTextFile = wx.TextCtrl(theMainFrame,pos=(5,5), size=(225,25))

theMainFrame.Show()
theApp.MainLoop()

