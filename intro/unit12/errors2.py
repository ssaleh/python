#errors2.py
import sys

def add(one,two):
    return int(one) + int(two)
def subtract(one,two):
    return int(one) - int(two)
def multiply(one,two):
    return int(one) * int(two)
def divide(one,two):
    if two != 0:
        return int(one) / int(two)
    else:
        print "you cannot divide by zero in this universe."
        raise ZeroDivisionError

def mathFunct(funct, x,y):
    print "The result is: ", funct(x,y)

def askForNumbers():
    num1 = raw_input("Please give me the first integer ")
    if not num1  or not num1.isdigit():
        print "You did not enter a valid first number."
        print "The program will now end."
        raise ValueError

    num2 = raw_input("Please give me the second integer ")
    if not num1  or not num2.isdigit():
        print "You did not enter a valid second number."
        print "The program will now end."
        raise ValueError, "You did not enter a second number"
    return num1, num2

choice = raw_input("What do you want to do? \nA = add, S = subtract, M = multiply and D = divide.")
if choice == 'A' or choice == 'S' or choice == 'M' or choice == "D":
    check1,check2 = askForNumbers()
else:
    print "This program requires you to choose an operation to perform. "
    print "You did not select A, S, M or D."
    print "Exiting now."
    sys.exit()
    
if not check1 or not check2:
    print "good bye!"
elif choice == 'A':
    mathFunct(add, check1,check2)
elif choice == 'S':
    mathFunct(subtract,check1,check2)
elif choice == 'M':
    mathFunct(multiply,check1,check2)
elif choice == 'D':
    mathFunct(divide,check1,check2)
else:
    print "You did not choose an operation to perform"
    print "Here are the numbers you entered: " , num1, " and ",  num2
     
