# warning_messages.py
''' This module demonstrates using warnings

    Warnings are similar to exceptions, but do not halt python
'''
import warnings

warnings.warn("Default user warning")
warnings.warn("Explicit user warning", UserWarning)
warnings.warn("This is obsolete", DeprecationWarning)
