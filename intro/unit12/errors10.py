try:
    raise Exception('spam', 'eggs')
except Exception, inst:
    print type(inst)     # the exception instance
    print inst.args      # arguments stored in .args
    print inst           # __str__ allows args to printed directly
    x, y = inst          # __getitem__ allows args to be unpacked directly
    print 'x =', x
    print 'y =', y

'''
<type 'exceptions.Exception'>
('spam', 'eggs')
('spam', 'eggs')
x = spam
y = eggs
'''
