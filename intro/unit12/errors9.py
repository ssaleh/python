#errors9.py

from myInputErrorClass import InputError


class Error9:
     def __init__(self, num1, num2,aDescription):
          try:
               self.x = int(num1)
               self.y = int(num2)
               self.str = aDescription
               return self.x // self.y
          except (ValueError, ZeroDivisionError, InputError, ValueError) ,e:
               print "---------------------------------------------------"
               print " An error has occured.",e
               print "---------------------------------------------------"
          else:
               print "This will execute only if no exception is raised"
          finally:
               print "This will execute no matter what"

b = Error9(4,0,"not a number")
 
                      

