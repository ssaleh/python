#errors7.py

from myInputErrorClass import InputError
from myDenomErrorClass import DenomError

class Error7:
     def __init__(self, num1 = 2, num2= 4,aDescription):
          try:
               self.x = int(num1)
               self.y = int(num2)
               self.str = aDescription
               return self.x // self.y
          except (ValueError, DenomError, InputError, ValueError),e:
               print "---------------------------------------------------"
               print " An error has occured.",e
               print "---------------------------------------------------"
          except Exception, e:
               print "the rest of the exceptions are caught here", e
          

     def printIt(self):
         '''I print the attributes of the object, but return nothing'''
         print "---------------------------------------------------"
         print "These are the object values."
         print "This is the first  number ", self.x
         print "This is the second number ", self.y
         print "This is the description ", self.str
         print "---------------------------------------------------"
	  
a = Error7("eee",0,"not a number")

a.printIt()

                      

