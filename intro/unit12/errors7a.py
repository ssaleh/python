#errors7.py
import sys
from myInputErrorClass import InputError
from myDenomErrorClass import DenomError

class Error7:
     def __init__(self, num1, num2,aDescription):
          try:
               self.x = int(num1)
               self.y = int(num2)
               self.str = aDescription
               self.divd = self.x // self.y
          except (ValueError, DenomError, InputError,AttributeError),e:
               print "---------------------------------------------------"
               print " An error has occured.",e
               print "---------------------------------------------------"
          except:
               print "hey any other type of error",e

     def printIt(self):
          '''I print the attributes of the object, but return nothing'''
          try:
              
              print "---------------------------------------------------"
              print "These are the object values."
              print "This is the first  number ", self.x
              print "This is the second number ", self.y
              print "This is the description ", self.str
              print "-------------------------------------------------"
          except:
               
              print "error has occured in printit" 
              
              
b = Error7(3,4,'this should work')

a = Error7("eee",0,"not a number")
print a
a.printIt()

                      

