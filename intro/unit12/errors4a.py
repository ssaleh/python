#errors4.py

from inputError import InputError
class Error4(object):
     "This class tests raising user defined exception objects "
     def __init__(self, num1, num2,aDescription):
          self.x = num1
          self.y = num2
          self.str = aDescription
          self.checkNegative()

     def printIt(self):
          '''I print the attributes of the object, but return nothing'''
          print "The first number you gave is: ", self.x
          print "The second number you gave is: ", self.y
          print "Here is your description: ", self.str
       
#Continued on the next page
 
     def return2Values(self):
         '''I return the two numbers'''
         return self.x, self.y
     def divide(self):
         '''I divide x by y, If y is 0 I raise an exception'''
         print "This function will divide the first number by the second"
         try:              
              return self.x // self.y
         except ZeroDivisionError, e:
              print "You cannot divide by zero!!!"
              print 'this is e', e
              self.y = 1
              return self.x // self.y

     def checkNegative(self):
          '''I check whether either number is negative'''
          if self.x < 0:
               raise InputError(self.x, "This class requires the first number to be positive")
          if self.y < 0:
               raise InputError(self.y, "This class requires the second number to be positive")


a = Error4(4,0,"test zero division")
a.printIt()
print a.divide()


