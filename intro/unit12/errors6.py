#errors6.py

from myInputErrorClass import InputError
from myDenomErrorClass import DenomError

class Error6:
     def __init__(self, num1, num2,aDescription):
          try:
               self.x = int(num1)
               self.y = int(num2)
               return self.x // self.y
          except ValueError:
               print "---------------------------------------------------"
               print "ValueError: This is not a valid number."
               print "---------------------------------------------------"
          except DenomError:
               print "Cannot divide by zero."
          except:
               print "Unexpected error"

     def printIt(self):
         '''I print the attributes of the object, but return nothing'''
         print "---------------------------------------------------"
         print "These are the object values."
         print "This is the first  number ", self.x
         print "This is the second number ", self.y
         print "This is the description ", self.str
         print "---------------------------------------------------"
	  
a = Error6("eee",0,"not a number")
a.printIt()

                      

