#files3
import sys
#program to accept a parameter to perform arithmetic

def add(one,two):
    return int(one) + int(two)
def subtract(one,two):
    return int(one) - int(two)
def multiply(one,two):
    return int(one) * int(two)
def divide(one,two):
    if two != 0:
        return int(one) / int(two)
    else:
        print "you cannot divide by zero in this universe."
        return 0

Notice = '''
================================================================================
 This program requires you to enter a parameter in order to perform an operation 
 If you did not choose -a (add), -s (subtract), -m (multiply), -d (divide) 
 then no operation will be performed.
================================================================================ '''

def mathFunct(funct, x,y):
    print "The result is: ", funct(x,y)

def askForNumbers():
    num1 = raw_input("Please give me the first integer ")
    if not num1  or not num1.isdigit():
        print "You did not enter a valid first number."
        print "The program will now end."
        return False,False

    num2 = raw_input("Please give me the second integer ")
    if not num1  or not num2.isdigit():
        print "You did not enter a valid second number."
        print "The program will now end."
        return False,False
    return num1, num2

print Notice

if len(sys.argv) > 1:
    choice = sys.argv[1]                            #takes in command line argument
    check1,check2 = askForNumbers()
else:
    print "This program requires a input parameter. "
    sys.exit()
    
if not check1 or not check2:
    print "good bye!"
elif choice == '-a':
    mathFunct(add, check1,check2)
elif choice == '-s':
    mathFunct(subtract,check1,check2)
elif choice == '-m':
    mathFunct(multiply,check1,check2)
elif choice == '-d':
    mathFunct(divide,check1,check2)
else:
    print "You did not choose an operation to perform"
    print "Here are the numbers you entered: " , num1, " and ",  num2
     
