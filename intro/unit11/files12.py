#files12.py

print "This is a quicker way to read a file line by line"
inpt = open(r'c:\python25\input.txt' , 'r+')

while 1:
    line = inpt.readline()
    if not line:
        break
    print line
inpt.close()  
