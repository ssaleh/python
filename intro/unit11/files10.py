#files10.py

inpt = open(r'c:\python25\input.txt' , 'r+')
gotString = inpt.read(5)
print "Read five characters: "
print "========================="
print gotString
print "========================="
gotString = inpt.read(16)
print "Read next sixteen characters: "
print "============================"
print gotString
print "========================="
gotString = inpt.read()
print "Read the rest of the file: "
print "============================"
print gotString
print "========================="
inpt.close()  
