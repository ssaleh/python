#files22.py
import pickle
import math
# the effect of this value is to repeat the string and the list 100 times to give a large piece of data
value = (  "this is a long string" * 100,[1.2345678, 2.3456789, 3.4567890] * 100)

# text mode
data = pickle.dumps(value)
print "The text pickle is this long: ", len(data) 

# binary mode
data = pickle.dumps(value, 1)
print "The binary pickle is this long: ",  len(data) 
