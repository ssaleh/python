while True:
    guess = raw_input("Guess my favorite drink or type q to quit: ") 

    if guess == "root beer" :
        print 'Congratulations, you guessed it.'
        break
    elif guess == "q" :
        print 'OK. Quitting now.'
        break
    elif guess == "milk" : 
        print 'No, it is a carbonated drink.'
    elif guess == "juice" : 
        print 'No, it is not a healthy drink.'
    else:
        print 'No, it is a brown colored carbonated beverage.'
else:
    print 'The while loop is over'

print 'Thanks for trying.'
