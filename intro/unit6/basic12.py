# basic12.py

'''
If the data structures include objects which are not fundamental Python
types, then the representation may not be loadable.
This may be the case if objects are those such as: files, sockets, classes, or
instances, as well as many other builtin objects which are not
representable as Python constants.

The formatted representation keeps objects on a single line if it can,
and breaks them onto multiple lines if they don't fit within the allowed width.
'''
import sys
import pprint

stuff = sys.path[:]
stuff.insert(0, stuff[:])
pp = pprint.PrettyPrinter(indent=4)
print "Normal Printing"
print stuff
print "Pretty Printed"
pp.pprint(stuff)

fruit_list = input('Enter a list containing some fruits: ')
if tuple(fruit_list):
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(fruit_list)
else:
    print "OK. Now let's try a tuple"
 
aTuple = input("Enter some values separated by commas: ")
if aTuple:
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(aTuple)
else:
    print "good bye"
