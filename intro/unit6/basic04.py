import random
random.seed()   				 
number = random.randint(1,6)   	 
while True:
        tryit = (raw_input('Guess what number between 1 and 6 I am thinking of: '))
        if tryit.isdigit():
            tryit = int(tryit) 
            if tryit == number:
                print 'Congratulations, you guessed it.'
                break
            elif tryit < 1:
                print 'No, it is a number between 1 and 6.'
            elif tryit > 6:
                print 'No, it is a number between 1 and 6.'
            elif tryit < number:
                print 'No, it is higher than that.'
            else:
                print 'No, it is lower than that.'
                
        else:
            print "You did not enter an integer"
