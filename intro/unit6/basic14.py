# basic14.py
'''Usually the repr() function and the str() function 
have the same output, but not with strings. '''
a = [3,4,5,"45445","the end is near"]
print "This is a:", a
print "This is str(a):", str(a)
print
b = 5
print "This is b:", b, " and this is str(b):" , str(b)
print
c = "The end is near"
print "This is a string: ", c
print "This is the str of that string: ", str(c)
