# flow02.py

a = 1
if a > 1:
    print "true"                

# nothing is printed by Python because the statement is false
if a < 2:
    print "ok now I print"

