# flow01.py
# test with numeric literals
if 2 > 2.1:
    print "no"

# nothing is printed by Python because the statement is false   
if 2 < 2.1:
    print "yes"
    print "this is a second line"

