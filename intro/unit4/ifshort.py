# shortif.py

a = 0
if a == 1: print 'The value of a is', a, 'as I expected'
# won't print

a = 1
if a == 1: print 'The value of a is', a, 'as I expected'
# will print

b = 3
if b > a: print 'The value of b is', b, 'which is greater than the value of', a
