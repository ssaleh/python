# Modules Lab Example
'''
Create a program called waitlist.py that will allow someone to keep track of a wait list.
The program should have the following options provided to the user running the program in a menu:
#1.  Print the list
#2.  Add a person to the list
#3.  Remove a person from the list
#4.  Check to see if a user is in the list
#5.  Exit the program

Take the file that you created in the Conditional Expressions lab (waitlist.py)
and create functions for each of the menu options.
Make the list variable global throughout your program and all other variables
local.

Create a file called modules.py. Copy the functions in functions.py into it. 
Now create another file called modulesTest.py and import the functions to use them
in a program.'''

aList = []
Menu1 = ''' Thank you for joining our Root Beer Club.
           Your order is important to us, and we will
           take your order when we come to your place
           in line.  '''

Menu2 = '''While waiting you have several options:
           =================================================
           Type 1 to print the entire list of people waiting
           Type 2 to add a name to our list.
           Type 3 to remove yourself from the list.
           Type 4 to check if a given person is waiting.
           Type 5 to exit.
           =================================================
           '''


def ChosePrint():
    print "=======================================================\n"
    print "This is the entire list of people waiting for Root Beer. \n", aList
    print "=======================================================\n"
    print 
    print Menu2
    
def AddNewName(name):
    aList.append(name)
    print "=======================================================\n"
    print "This is the entire list now that you have added ", name
    print aList
    print "=======================================================\n"
    print Menu2
   
def RemoveName(name):
    for x in range(len(aList)):
        if aList[x] == name:
            del aList[x] 
    print aList
    print Menu2

def CheckName(name):
    for x in range(len(aList)):
        if aList[x] == name:
            print "You are checking for " , aList[x] 
            print "Here is the entire list \n", aList
    print Menu2

def MenuProcess():
    print Menu1
    print Menu2
    choice = raw_input("What would you like to do? " )
    while choice != '5':
        if choice == '1':
            ChosePrint()
            choice = raw_input("What would you like to do? " )
            continue
        elif choice == '2':
            AddOne = raw_input("Please enter the name you wish to add ")
            if not AddOne:
                AddOne = raw_input( "Do you want to quit or add a person? Press 5 to quit. ")
                if AddOne == '5':
                    print "Goodbye!"
                    break
                else:
                    AddNewName(AddOne)
                    choice = raw_input("What would you like to do? " )
                    continue
            else:
                AddNewName(AddOne)
                choice = raw_input("What would you like to do? " )
                continue
        elif choice == '3':
            RemOne = raw_input("Please enter the name you wish to remove ")
            if not RemOne:
                RemOne = raw_input( "Please give the name of the person you wish to remove or 5 to quit. ")
                if RemOne == '5':
                    print "Goodbye!"
                    break
                else:
                    RemoveName(RemOne)
                choice = raw_input("What would you like to do? " )
                continue        
            else:
                RemoveName(RemOne)
                choice = raw_input("What would you like to do? " )
                continue        
        elif choice == '4':
            ChkOne = raw_input("Please enter the name you wish to check for ")
            if not ChkOne:
                ChkOne = raw_input( "Please give the name of the person you wish to check for or 5 to quit. ")
                if ChkOne == '5':
                    print "Goodbye!"
                    break
                else:
                    CheckName(ChkOne)
                    choice = raw_input("What would you like to do? " )
                    continue        
            else:
                CheckName(ChkOne)
                choice = raw_input("What would you like to do? " )
                continue        
        elif choice == '5':
            print "Good bye!"
        else:
            print "=========================================="
            print "= You did not choose 1 - 5!!!            ="
            print "= You will now exit the program          ="
            print "= Goodbye!                               ="
            print "=========================================="
            break
