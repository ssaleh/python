# classes Test program
'''Return to your modules.py program and its modulesTest.py program.
Now convert the functions in modules.py into a class. Make the list and menu global variables into class wide attributes. The functions become methods.
Test this class to make sure the menu and its options still work correctly. 
'''

from classes import myListWaits

print("Do you want to run a program that allows you to create and maintain a list?")
runIt = raw_input('Y or N?')

if runIt == 'Y' or runIt == 'y':
    M = myListWaits()
    M.MenuProcess()
else:
    print "OK. Well maybe you wanted a tuple?!! Well we don't have one here."
    print "Goodbye!"
    
