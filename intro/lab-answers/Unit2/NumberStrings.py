# Number Strings example
'''
1. Create a simple file named NumberStrings.py. 
2. Inside this file place statements that add, subtract, multipy and divide
the numbers 334 and -45. 
3. Print out the results. What happens with the divison? Does it divide evenly?
If not, what happens to the remainder? 
4. Add the Python function to get the remainder of division. Print the results. 
5. Create a string that repeats the word "printout" 20 times. 
6. Create 3 strings and print them out concatenated together.
What operators can you use to do this?
7. Create a long string "The man in the moon is made out of green cheese."
Then print out slices of this string that include only "moon" and "green cheese".
8. Use a string method on the above string to count the number of e's used in the string.
How many are there?
9. Use another string method on the same string to change all the m's to b's.
Now change n's to t's. What do you get?'''

print 334 + (-45)
print 334 - (-45)
print 334 * (-45)
print 334 / (-45)
# the division above gave -8 (the floor or lower integer) rather than -7 which is what
# I expected. Since these are integers I got a remainder, which can be found using % operator 
print 334 % (-45)

print "printout"*5                                      #use multiplication operator
print "one is " + "attached to " + "the other string"   #use concatenation operator
# print some substrings
aString = "The man in the moon is made out of green cheese."
print aString[15:19]
print aString[35:-1]
# print all e in string
print aString.count('e')
print aString
print aString.replace('m','b')
print aString.replace('n','t')


