# Class Lab Example
'''
---conditional expressions Lab is waitlist.py
Create a program called waitlist.py that will allow someone to keep track of a wait list.
The program should have the following options provided to the user running the program in a menu:
#1.  Print the list
#2.  Add a person to the list
#3.  Remove a person from the list
#4.  Check to see if a user is in the list
#5.  Exit the program

---functions.py
Take the file that you created in the Conditional Expressions lab (waitlist.py)
and create functions for each of the menu options.
Make the list variable global throughout your program and all other variables
local.

---modules.py
Create a file called modules.py. Copy the functions in functions.py into it. 
Now create another file called modulesTest.py and import the functions to use them
in a program.

---classes.py 
Return to your modules.py program and its modulesTest.py program.
Now convert the functions in modules.py into a class. Make the list and menu global variables into class wide attributes. The functions become methods.
Test this class to make sure the menu and its options still work correctly.

--files.py
Return to the classes.py and classesTest.py programs.
Add functions to this class to:
write out the wait list to a new or existing file (can have option to overwrite).
read in a file of names and add them to the list.
open and search a file of names to add specified ones to the list. 
Add code to test these new functions.
'''
class myListWaits():
    
    def __init__(self):
        
        self.aList = []
        self.Menu1 = ''' Thank you for joining our Root Beer Club.
               Your order is important to us, and we will
               take your order when we come to your place
               in line.  '''

        self.Menu2 = '''
               While waiting you have these options:
               =====================================================
               = Type 1 to print the entire list of people waiting  
               = Type 2 to add a name to our list.                  
               = Type 3 to remove yourself from the list.
               = Type 4 to check if a given person is waiting.
               = Type 5 to exit.
               = Type 6 to write out the list to a file.
               = Type 7 to read in a text file and create a new list.
               ======================================================
               '''
    def ChosePrint(self):
        print "=======================================================\n"
        print "This is the entire list of people waiting for Root Beer. \n", self.aList
        print "=======================================================\n"
        print 
        print self.Menu2
        
    def AddNewName(self,name):
        self.aList.append(name)
        print "=======================================================\n"
        print "This is the entire list now that you have added: ", name
        print self.aList
        print "=======================================================\n"
        print self.Menu2
       
    def RemoveName(self,name):
        for x in range(len(self.aList)):
            if self.aList[x] == name:
                del self.aList[x] 
        print self.aList
        print self.Menu2

    def CheckName(self,name):
        for x in range(len(self.aList)):
            if self.aList[x] == name:
                print "You are checking for " , self.aList[x] 
                print "Here is the entire list \n", self.aList
        print self.Menu2

    def WriteOut(self):
        filePath = raw_input("what is the full path and name of the file to write to?")
        overWrite = raw_input("Do you want to append to this file (a) or overwrite it (w)?")
        if overWrite != 'a' and overWrite != 'w':
            print "you have not chosen a correct response. The file will be appended to."
            overWrite = 'a'
        fileName = open(filePath,overWrite)
        for x in self.aList:
            fileName.write(x)
        print "The names in your list are now in the file:\n " ,filePath
        fileName.close()
         
        
    def MenuProcess(self):
        print self.Menu1
        print self.Menu2
        choice = raw_input("What would you like to do? " )
        while choice != '5':
            if choice == '1':
                self.ChosePrint()
                choice = raw_input("What would you like to do now? " )
                continue
            elif choice == '2':
                AddOne = raw_input("Please enter the name you wish to add ")
                if not AddOne:
                    AddOne = raw_input( "Do you want to quit or add a person? Press 5 to quit. ")
                    if AddOne == '5':
                        print "Goodbye!"
                        break
                    else:
                        self.AddNewName(AddOne)
                        choice = raw_input("What would you like to do now? " )
                        continue
                else:
                    self.AddNewName(AddOne)
                    choice = raw_input("What would you like to do now? " )
                    continue
            elif choice == '3':
                RemOne = raw_input("Please enter the name you wish to remove ")
                if not RemOne:
                    RemOne = raw_input( "Please give the name of the person you wish to remove or 5 to quit. ")
                    if RemOne == '5':
                        print "Goodbye!"
                        break
                    else:
                        self.RemoveName(RemOne)
                    choice = raw_input("What would you like to do now? " )
                    continue        
                else:
                    self.RemoveName(RemOne)
                    choice = raw_input("What would you like to do now? " )
                    continue        
            elif choice == '4':
                ChkOne = raw_input("Please enter the name you wish to check for ")
                if not ChkOne:
                    ChkOne = raw_input( "Please give the name of the person you wish to check for or 5 to quit. ")
                    if ChkOne == '5':
                        print "Goodbye!"
                        break
                    else:
                        self.CheckName(ChkOne)
                        choice = raw_input("What would you like to do now? " )
                        continue        
                else:
                    self.CheckName(ChkOne)
                    choice = raw_input("What would you like to do now? " )
                    continue        
            elif choice == '5':
                print "Good bye!"
                break
            elif choice == '6':
                self.WriteOut()
                choice = raw_input("What would you like to do now? " )
                continue  

            elif choice == '7':
                filePath = raw_input("what is the full path and name of the file?")
                fileName = open(filePath,'r')
                for fileLine in fileName:
                    self.aList.append(fileLine)
                fileName.close()
                print "the names in your file were added to the list:\n " ,self.aList
                choice = raw_input("What would you like to do now? " )
                continue  
            else:
                print "=========================================="
                print "= You did not choose 1 - 7!!!            ="
                print "= You will now exit the program          ="
                print "= Goodbye!                               ="
                print "=========================================="
                break


                           
                        
                           
                           
                           
                           

    
    

 
