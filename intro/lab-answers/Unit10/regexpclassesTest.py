# regexpclasses Test program
'''Return to classes.py which is now a class containing the menu functions.
Add functions to this class to search the list using wildcards such as:
all names beginning with a given letter
all names ending with a given letter
all names of a certain length
Add code to test these new functions.
'''

from regexpclass import myListWaits

print("Do you want to run a program that allows you to create and maintain a list?")
runIt = raw_input('Y or N?')

if runIt == 'Y' or runIt == 'y':
    M = myListWaits()
    M.MenuProcess()
else:
    print "OK. Well maybe you wanted a fruitcake?!! Well we don't have one here."
    print "Goodbye!"
    
