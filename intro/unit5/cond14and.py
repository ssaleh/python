#!/usr/bin/python
# cond14and.py
a,b = 5,10
print "this is what you get when you print a and b " , a and b

if (a == 5) and (b == 10):
    print "yes a == 5 and b == 10" 

if (a == 5) and (b == 99):
    print "yes a == 5 and b == 99"
else:
    print "no either a != 5 or b != 99"


