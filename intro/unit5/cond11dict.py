﻿#!/usr/bin/python
# cond11dict.py
# create two different dictionaries
phoneBook = {'allen':4841235, 'molly':5389797, 'bob':3441245, 'zak':5678787}
print phoneBook
anotherBook = {'allen':334343, 'zak':343434}
# show that Python can evaluate them but they are not equal
print phoneBook == anotherBook
# remove the key/value pair with molly’s key and show what keys remain
del phoneBook['molly']
del phoneBook['bob']
print phoneBook == anotherBook
phoneBook['allen']=334343
phoneBook['zak']=343434
print phoneBook == anotherBook
print phoneBook is anotherBook
blackBook=phoneBook.copy()
redBook = phoneBook
print redBook is phoneBook
print blackBook is phoneBook
print phoneBook
print phoneBook.keys()

