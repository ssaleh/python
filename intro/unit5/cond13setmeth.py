#!/usr/bin/python
# cond13setmeth.py 
colors=set(['white', 'black', 'red', 'green', 'blue'])
print 'Here is the colors set:', colors
primary=set(['red','green', 'blue'])
print 'Here is the primary set:', primary
print 'Is colors a superset of primary?', colors.issuperset(primary)
print 'Is primary a subset of colors?', primary.issubset(colors)

