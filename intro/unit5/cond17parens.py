#!/usr/bin/python
# cond17parens.py

a,b,c =5,10,50
print "Here are the values of a: %s, b: %s, and c: %s" %  (a,b,c)
#either first two conditions must both be true OR last condition must be
#true to make entire conditional statement true: 
if (a == 70) and (b == 10) or (c == 50):
    print "Either c == 50, or both a == 70 and b == 10"

#first condition must be true AND one of the last two conditions must be
#true to make entire conditional statement true: 
if (a == 70) and ((b == 10) or (c == 50)):
    print "yes"
else:
    print "Either a != 70 or either b != 10 or c != 50"

