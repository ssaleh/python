#!/usr/bin/python
# cond06streq.py

# Two different references and one value
Lunch = "bread soda"
Breakfast = "bread soda"
print "Is Lunch == Breakfast?", Lunch == Breakfast
print "Is Lunch > Breakfast?", Lunch > Breakfast
print "Is Lunch < Breakfast?", Lunch < Breakfast
print "So, Lunch is Breakfast?", Lunch is Breakfast
 
