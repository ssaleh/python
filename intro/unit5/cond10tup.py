#!/usr/bin/python
# cond10tup.py
aTuple1 =  1,30,(444,666)
print "the first tuple is ", aTuple1
aTuple2 = (1, 30, (9, 30))
print "the second tuple is ", aTuple2
aTuple3 = (1, 30, (444, 'Hello'))
print "the third tuple is ", aTuple3
aTuple4 = (1, 30, (9, 30))
print "the fourth tuple is" , aTuple4
print "is the first one < the second one? ", aTuple1 < aTuple2
print "is the first one >= to the third one? ",aTuple1 >= aTuple3 
print "are the two values equal?" , aTuple1 == aTuple2
print "are 1 and 2 the same reference? " , aTuple1 is aTuple2 
print "are 4 and 2 the same reference? " , aTuple4 is aTuple2 
print "is this fourth tuple == to the second one? ", aTuple4 == aTuple2
