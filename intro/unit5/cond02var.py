#!/usr/bin/python
# cond02var.py

a,b = 2.002, 2
if a == b:
    print "they are equal"
else:
    print "so close but not equal"

if a < b:
    print "Wow! I don't think so!"
else:
    print "Just as I thought."
