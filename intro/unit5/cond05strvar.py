#!/usr/bin/python
# cond05strvar.py

# Two different references and two different values
Lunch = "bread soda"
Breakfast = "bread coffee"
print "Is Lunch == Breakfast?" , Lunch == Breakfast
print "Is Lunch > Breakfast?" ,Lunch > Breakfast
print "Is Lunch < Breakfast?" ,Lunch < Breakfast

# now make the two refer to the same string
Lunch = Breakfast
print "Is Lunch == Breakfast NOW?" , Lunch == Breakfast
