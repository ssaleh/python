#!/usr/bin/python
# cond16not.py
a = 'one'
b = [1,2]
c = 0
print 'Here is a: %s, b: %s, and c: %s'  % (a,b,c)
print 'Here is not a: %s, not b: %s, and not c: %s' % (not a,not b,not c)

