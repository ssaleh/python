def functions15(list_size, incremt = 2, aList = [355, 45, '45']):
	print "parameters are: " , list_size, incremt, aList
	size = 0
	while size < list_size:
		aList.append(size)
		size = size + incremt
	return aList

print "this is what was used to call the function list_size = 12"   
myResult = functions15(list_size = 12)
print "this is what is returned: ", myResult

myResult = functions15(incremt=3,list_size=1)
print "this is what is returned: ", myResult
