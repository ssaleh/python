# functions12.py
def functions12(queue):
    '''Return next person in queue'''
    return queue.pop(0)

lineup = ['Mary', 'Tom', 'Sue', 'Bill']

print "Here's the current lineup:", lineup
nextup = functions12(lineup)
print "The next person up is:", nextup
print "The new lineup is:", lineup
