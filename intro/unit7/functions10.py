# functions10.py

def functions10(list_size, increment):
    aList = []
    size = 0
    if list_size == 0:
        return 0
    if increment < 0:
        return 'uggg'
    while size < list_size:
        aList.append(size)
        size = size + increment
    aTuple = tuple(aList)
    return aTuple 

L = 36
I = 3.141592
 
myResult = functions10(L,I)
c = type(myResult)
if c == tuple: 
    print "this is a tuple"
elif c == int:
    print "this is an integer"
elif c == str:
    print "this is a string"
else:
    print "wow! I didn't expect this result"


     
