# functions20.py

def factorial(n):
    if n == 1:
        return 1
    else:
        return n * factorial(n-1)

def facts(times):
    print "The factorial of %d is %d digits long" % (times, len(str(factorial(times))))

facts(100)
facts(998)
# the normal recursionlimit is 1000
from sys import getrecursionlimit, setrecursionlimit, maxint
print 'The recursion limit is:',getrecursionlimit()
setrecursionlimit(maxint)
print 'The new recursion limit is:',getrecursionlimit()
facts(10000)
