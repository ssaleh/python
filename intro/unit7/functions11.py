# functions11.py
def functions11(alpha, beta):
    alpha, beta = 10, 1 # change the values of the parameters
    print "Inside the function alpha and beta are now:",alpha,beta
    return alpha, beta

alpha,beta = 'ten','one'
print "Outside before calling the function, alpha and beta are:",alpha,beta  
myResult = functions11(alpha,beta)
print "Returned value from function: ", myResult
print "Outside after calling the function, alpha and beta are:",alpha,beta  

