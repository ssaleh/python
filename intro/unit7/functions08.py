# functions08.py
def functions8():
    aList = []
    size = 0
    while size < 7:
        aList.append(size)
        size = size + 3.141592
    aTuple = tuple(aList)
    return aTuple + (5,6,7,6.777)
           
myResult = functions8()
print "This tuple was returned by a function: ", myResult
for x in myResult:
    print "tuple element is  %.2f" % x


