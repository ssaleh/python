#docstrings.py
def showdoc():
    '''My first function using a docstring

    This function takes no arguments and returns None
    It prints out a message
    '''
    print "this is my first documented Python function  "
    return 

showdoc()
print showdoc.__doc__
