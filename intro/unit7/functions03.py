# functions03.py
def functions3():
    '''My first function using a docstring

    This function takes no arguments and returns None
    It prints out a message
    '''
    print "this is my first documented Python function  "
    return

functions3()
print functions3.__doc__
