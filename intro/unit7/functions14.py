# functions14.py
def functions14(list_size, incremt = 1, aList = [355, 45, '45']):
	print "parameters list and incremt are: " , list_size, incremt, aList
	size = 0
	while size < list_size:
		aList.append(size)
		size = size + incremt
	return aList

a = 15
b = 3   
c = [4,5,6,888,'rootbeer']  
print "this is what was used to call the function ", a      
myResult = functions14(a)
print "this is what is returned: ", myResult

print "this is what was used to call the function ", a, b      
myResult = functions14(a,b)
print "this is what is returned: ", myResult

print "this is what was used to call the function ", a, b, c      
myResult = functions14(a,b,c) 
print "this is what is returned: ", myResult
myResult = functions14(a,aList=[1,2,3])
