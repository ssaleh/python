# functions04.py
def functions4():
    '''This function returns a value'''
    a, b = 0,1
    if a > b:
        return a
    else:
        return b

#this statement calls the function and stores the result
results = functions4()
#this statement prints out the result
print "this is the value I got returned from the function: ", results
