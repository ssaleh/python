menu = '''
    =====================================================================
    = Give me two integers and a function to perform.
    = Choose A or a for Add them
    = Choose S or s for Subtract the second integer from the first one.
    = Choose M or m for Multiply them
    = Choose D or d to get integer Division
    = Choose R or r to get the remainder of integer Division
    ======================================================================
    '''

def add(one,two):
    return int(one) + int(two)
def subtract(one,two):
    return int(one) - int(two)
def multiply(one,two):
    return int(one) * int(two)
def divide(one,two):
    if two != 0:
        return int(one) / int(two)
    else:
        print "you cannot divide by zero in this universe."
        return 0
    
def remainder(one,two):
     if two != 0:
        return int(one) % int(two)
     else:
        print "you cannot divide by zero in this universe."
        return 0


# the actual function to be performed is passed in here as 'funct' and used
# to call the correct function using the two integers
def functions13(funct, x,y):
    print funct(x,y)

def askForNumbers():
    num1 = raw_input("Please give me the first integer ")
    
    if not num1 :
        print "you did not enter a valid first number."
        print "The program will now end. Bye!"
        return False,False

    num2 = raw_input("Please give me the second integer ")
    if not num2 : 
        print "you did not enter a valid first number."
        print "The program will now end."
        return False,False
    return num1, num2

print menu
choice = raw_input("What would you like to do? " )
if choice == 'q' or choice == 'Q':
    print "sorry that you don't want to do some math here!"
    
while choice != 'Q' and choice != 'q':
    check1,check2 = askForNumbers()
    if not check1:
        print "good bye!"
        choice == 'Q'
        break
    elif choice == 'A' or choice == 'a':
        functions13(add, check1,check2)
        choice = raw_input("What would you like to do? " )
        continue
    elif choice == 'S' or choice == 's':
        functions13(subtract,check1,check2)
        choice = raw_input("What would you like to do? " )
        continue
    elif choice == 'M' or choice == 'm':
        functions13(multiply,check1,check2)
        choice = raw_input("What would you like to do? " )
        continue
    elif choice == 'D' or choice == 'd':
        functions13(divide,check1,check2)
        choice = raw_input("What would you like to do? " )
        continue
    else:
        print "Good Bye!"
        choice == 'Q'
        
        
