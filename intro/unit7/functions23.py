# functions23.py
from math import sqrt

nums = (2,4,6,8,9,15,16)

def squared(num): return sqrt(num) == int(sqrt(num))
print filter(squared, nums)

print filter(lambda x: sqrt(x) == int(sqrt(x)), nums)
