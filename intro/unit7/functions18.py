def functions18(list_size):
       global size          			
       size += 1
       aNumber = 12         		
       myResult = list_size+14    
       print locals()
       return myResult

size = 27       			# accessed in function and declared global  
aNumber = 17    			# not accessed in function
a = 16          			# accessed in function since used to call function
 
print "this is the result of calling the function:", functions18(a) 
print "this is the value of size a global variable:" , size
print "this is the value of the aNumber in the MAIN program:", aNumber
print globals()
