# functions22.py

numlist = [2,4,6,8]

def square(num): return num * num
print map(square, numlist)

print map(lambda x: x * x, numlist)
# numlist is not modified unless numlist=map(lambda x: x * x, numlist)
print numlist 
