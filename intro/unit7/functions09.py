# functions09.py
def functions9(list_size):
    aList = []
    size = 0
    while size < list_size:
        aList.append(size)
        size = size + 3.141592
    aTuple = tuple(aList)
    return aTuple  

L = 10           
myResult = functions9(L)
print "This result was created with a list size of", L, "and we got "
for x in myResult:
    print "tuple element is  %.2f" % x


