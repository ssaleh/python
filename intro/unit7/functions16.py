# functions16.py
def functions16(*args):
    return args
    aList = []
    for value in args:
        aList.append(value)
    return aList
   
a = "string"
b = 4
c = [4,5,6]
d = ("one", 3)

myResult = functions16(a)
print "this is what is returned: ", myResult

myResult = functions16(b,c)
print "this is what is returned: ", myResult

myResult = functions16(b,c,d)
print "this is what is returned: ", myResult

myResult = functions16(4, b,c,d)
print "this is what is returned: ", myResult

myResult = functions16(4, d,c,b)
print "this is what is returned: ", myResult

