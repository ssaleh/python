#Variables and Data Structures Example
'''
1. Create a simple file named Variables.py.  
2. Inside this file place statements to create the following variables:
  a, which is an integer with a value of 45.
  b, which is another integer with a value of 334.
  c, which is a floating point number with a value of 45.001
3. Now using the variable names add them together, multiply them and divide them.
Is there a difference when you divide 334 by 45 and when you divide it by 45.001?
Print out results.
4. Now let's create a list, aList, from the range 1-10.
What technique did you use to you this?
What do the results look like when you print them?
5. Add 3 more values (100, 200 and 300) to your list. 
6. Make a new list from aList, containing only its 3-6 items.
What does this list contain? 
7. Now convert your list into a tuple.
'''

a = 45
b = 334
c = 45.001

print a+b
print a+c
print b+c
# 334 - 45 and 334- 45.001
print b-a
print b-c

print a-c

# Now we get to see how Python's display of floating point numbers is confusing
print a * c
print a * b

# 334 / 45 and 334/ 45.001 give very different answers!
print b/a
print b/c

aList = range(1,10)
print aList
aList.append(100)
aList.extend([200,300])
print "aList extended with 200 and 300", aList
aList2 = aList[3:7]
print "aList sliced with 3:7 into aList2", aList2
aTuple = tuple(aList2)
print aTuple
