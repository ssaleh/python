#!/usr/bin/python
#dataStruct2.py

# The array
y = [1,2.0,(3,4),[5],{"Hello":"World"},2, 3,4,5]
print y

# delete the second element from the array
del y[2]
print y

# set the  first element  of the array to none
y[0] = None
print y

# Pop or print the first item only
z = y.pop(0)
print z
print y


print "length is ", len(y)  # function that takes list argument
print "where is list of 5? ", y.index([5])
print "how many 2s are there? ", y.count(2)
print y[-3:]
y.reverse()
print y
y.sort()
print y

 


