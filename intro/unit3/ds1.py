#!/usr/bin/python

#dataStruct1.py

fruit = ['apple','banana','mango','orange']
print fruit
print fruit[1]
print

x = [1,2,3,4,5]

# The following implies start at 45, count to 3, by negative 3"
z = range(45,3,-3)
print z
print

# Element nuumbers start at 0, not 1
test = [0,3,4,5,66.5657,['aba',3,'45'],3,'a']
print "5th element",test[5]

# Remember Strings start at zero as well.
print "string in that list", test[5][2]
print
print
y = [1,2.0,(3,4),[5],{"Hello":"World"}]
print "y = ", y

# print 2 field from the right
print "y[-2]] = ", y[-2]
print "y[-3]] = ", y[-3]
y.append('asdfalksdjfa')
print "y (with appended data) = ", y, "\n"
print "\n\n\n"
y.append( (4,5,6,67,'455') )
print y
y.insert(2,'4545')  #insert this as element where I tell you to
print y
y.append(y)  # insert this as element into the list at end
y.extend(y)  # concatenates two lists
print y
z = y + y
print z
print
print
z = ['a','e',3,5,'rootbeer',('e3',5)]
print z * 3
print z[1:3]
