#dataStruts6.py

sarah = 1
bill = 2
joe = 3

phone =  {sarah:33445,bill:3333,joe:3366}
print phone
# {1: 33445, 2: 3333, 3: 3366}

q = [54,67,87,8,8]
r = (3,5,6,7)
s = 'ddddd'
phone2 = {sarah:q,bill:r,joe:s}
print phone2
# {1: [54, 67, 87, 8, 8], 2: (3, 5, 6, 7), 3: 'ddddd'}

sara = 'george'
billy = 'bob'
Jo = 'july'
phone3 = {sara:1111/333,billy:4/5,Jo:7/-3}
print phone3
#'bob': 0, 'july': -3, 'george': 3}

phone['ben'] = 13690
print phone
# {1: 33445, 2: 3333, 3: 3366, 'ben': 13690}

phone[(3,5,6,76)] = 77666
print phone
#{1: 33445, 2: 3333, 3: 3366, 'ben': 13690, (3, 5, 6, 76): 77666}

print phone[1]
#33445
print phone['ben']
#13690
print phone[(3, 5, 6, 76)]
#77666

phone2 = {sarah:q,bill:r,joe:s}
print phone2[sarah]
#[54, 67, 87, 8, 8]
print q
#[54, 67, 87, 8, 8]
print phone2[sarah] [3]
#8
print phone2.keys()
#[1, 2, 3]
print phone.keys()
#[1, 2, 3, 'ben', (3, 5, 6, 76)]
