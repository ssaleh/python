# class05.py
'''Static method can be called with or without an instance'''
class StatusQuo(object):
    status = 'no change'
    def Stat():
        return StatusQuo.status
    Stat=staticmethod(Stat)
    
class StatusDyn(StatusQuo):
    @staticmethod # same with decorator
    def Stat():
        return False

print StatusQuo.Stat()    
s = StatusQuo()
print s.Stat()

print StatusDyn.Stat()
d = StatusDyn()
print d.Stat()
