class A(object):
    def __init__(self, x, y):
        print 'init A'
        self.x = x
        self.y = y
    
    @classmethod
    def from_str(cls, string):
        return cls(*map(float, string.split(',')))


class B(A):
    def __init__(self, x, y):
        print 'init B'
        A.__init__(self, x, y)


def main():
    B.from_str('42,23')
