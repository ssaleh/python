#access2.py

class Student(object):					 
      count = 1
      firstName = "Joe"
      lastName = "Smith"
      listOfCourses = ["Accounting", "Math", "English", "Econ"]  


print Student.__dict__
#Student.__dict__['count'] = 2
# results in TypeError: 'dictproxy' object does not support item assignment
setattr(Student, 'count', 2)
print Student.__dict__
