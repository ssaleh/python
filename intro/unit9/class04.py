# class04.py
class ClassMethods(object):
    '''Class to demonstrate class methods'''
    z = 'three' # class attribute
    def __new__(cls, x, y):
        print '__new__ method called'
        cls.x = x 
        cls.y = y
        return object.__new__(cls, x, y)
    def __init__(self, x, y): 
        '''__init__ is called after __new__'''
        print '__init__ method called'
        self.x = str(x)
        self.y = str(y)
    def __del__(self): 
        '''__del__ method is called automatically or by del'''
        print '__del__ method called'
    def __str__(self): 
        '''__str__ called when str or print function used'''
        stringval = 'x: ' + str(self.x) + ', y: ' + str(self.y) + ', z: ' + str(self.z)
        return stringval
    def __repr__(self): 
        '''__repr__ called when repr function is used'''
        stringval = 'ClassMethods object: ' + str(self.__dict__)
        return stringval
    def __getattr__(self, item):
        '''__getattr__ called when retrieving an attribute value
        
        Method would normally raise an AttributeError'''
        try:
            return self.__dict__[item]
        except:
            return None
    def __setattr__(self, item, value):
        '''__setattr__ called to set an item to it's value'''
        if item != 'z':
            self.__dict__[item]=value
        # Prevent changing attribute z
    def __eq__(self, other):
        '''__eq__ called when comparing equality of two instances'''
        if self.x == other.x and self.y == other.y:
            return True
        else:
            return False
        
c = ClassMethods('one',2)
print dir(c)
print c
print repr(c)
print dir(ClassMethods)
print c.dne
c.z = 'test'
print c.z
c.Z = 'test'
print c.Z
d = ClassMethods(1,'two')
print d == c
e = ClassMethods(1,'two')
print d == e
del c # even though only c is explicitly deleted __del__ is called three times
