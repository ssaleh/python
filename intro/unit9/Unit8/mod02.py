#!/usr/bin/python

# mod02.py
import sys
# one module might import other modules
def showmaxint():
    print "Here is the sys.maxint:"
    print sys.maxint

def showpath():
    print "Here is the sys.path:"
    print sys.path
if __name__ == '__main__':
    showmaxint()
    showpath()
# call the function
