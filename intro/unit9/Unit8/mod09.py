# mod09.py
'''A simple module with functions only

This module shows a couple of functions from the sys module'''
import sys
# one module might import other modules
def showpath():
    '''Show sys.path where modules can be found'''
    print "Here is the sys.path:"
    print sys.path
def showmaxint():
    '''Show the largest non-long integer supported'''
    print "Here is the sys.maxint:"
    print sys.maxint

