# mod06.py
from mod01 import showpath
from mod01 import showmaxint
# could have also used:
# from mod01 import showmaxint, showpath
# now this functions are imported into the main name space
showmaxint()
showpath()

