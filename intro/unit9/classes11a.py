class Class11:
    ''' this class has several data and functional attributes '''
    listOfCourses = ["Accounting", "Math", "English", "Econ"]
        
    def addClass(self, classname):
        self.listOfCourses.append(classname)
         
    def __str__(self):
        x =  " ".join(self.listOfCourses)
        return "These are your classes: " +  x 


 
