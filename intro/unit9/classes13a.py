#classes13.py
from classes12 import Class12
class Class13(Class12):
        i = 2
        "This is a derived class from Class12"
        def setNumber(self, x):
                '''I set the numeric value'''
                self.i = x
                print "class12", self.i, "this is class13", Class13.i
                return        

        def setString(self, y):
                '''I set the string value'''
                self.str = y
                print "setString"
                return

b = Class13(3,'dog')
b.setNumber(4)
