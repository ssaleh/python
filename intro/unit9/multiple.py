class GrandParent(object):
    def show(self):
        print 'Grand Parent'

class MomParent(GrandParent):
    gender = 'female'
    pass
    
class DadParent(GrandParent):
    gender = 'male'
    def show(self):
        print 'Dad Parent'

class Child(DadParent,MomParent):
    pass
    
c = Child()
c.show()
print 'Inheritence is from left to right, so gender is: ', c.gender
