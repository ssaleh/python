#Decorator.py

class ClassMethod(object):
    @classmethod 
    def printCls(cls):
        print "This is a classmethod of ", cls

ClassMethod.printCls()
a = ClassMethod()
a.printCls()

class StaticMethod(object):
    @staticmethod
    def printStatic():
        print "This is a static method"

StaticMethod.printStatic()
b = StaticMethod()
b.printStatic()

     
