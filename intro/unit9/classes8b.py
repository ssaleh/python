class Class1:
    i = 1
    def add(self,one,two):
        return one + two
    def subtract(self,one,two):
        return one - two
    def multiply(self,one,two):
        return one * two
    def divide(self, one, two):
        if two != 0:
            self.i = self.i + 1
            return one / two
        else:
            return "cannot divide by zero"
    def complexity(self,one,two):
        self.add(one,two)
print "---------------------------- Classic Class -----------------------------"
print Class1.__dict__
print
print

class Class2(object):
    i = 1
    def add(self,one,two):
        return one + two
    def subtract(self,one,two):
        return one - two
    def multiply(self,one,two):
        return one * two
    def divide(self, one, two):
        if two != 0:
            return one / two
        else:
            return "cannot divide by zero"
    def complexity(self,one,two):
        self.add(one,two)
print "---------------------------- New Class -------------------------------"
print Class2.__dict__
