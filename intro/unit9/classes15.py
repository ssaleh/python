#classes15.py
from classes13 import Class13
from classes11a import Class11

class Class15(Class13, Class11):
    "This is a derived class from Classes 11 and 13"
    
    def __init__(self, num, aDescription,x):
        Class13.__init__(self,num,aDescription)
        self.setWeight(x)
       
    def setWeight(self, x):
        '''I set the class values'''
        self.wt = x
    def printOut():
        Class12.printIt()
        print "Your weight is ", self.wt
 

