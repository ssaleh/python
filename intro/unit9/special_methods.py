#special_methods.py

class Point(object):
    def __init__(self, one = 0, two = 0):
        self.x = one
        self.y = two
    def __str__(self):
        s =  "the Point is found at x = " +  str(self.x) + " and y = " + str(self.y)
        return s 

p1 = Point(3,4)
print p1.__dict__

print p1         
