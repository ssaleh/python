# class03.py

class Student(object):
    def __init__(self,first,last):
        self.firstName = first
        self.lastName = last
        self.listOfCourses = ["Accounting", "Math", "English", "Econ"]

    def addClass(self, classname):
        self.listOfCourses.append(classname)

a = Student("Joe","Smith")
a.addClass("PE")
print "These are the object attributes:"
print a.firstName, a.lastName, a.listOfCourses
