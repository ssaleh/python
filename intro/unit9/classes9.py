class Class9:
    def __init__(self):
        self.count = 1
        self.firstName = "Joe"
        self.lastName = "Smith"
        self.listOfCourses = ["Accounting", "Math", "English", "Econ"]
        
    def addClass(self, classname):
        self.listOfCourses.append(classname)

a = Class9()
a.addClass("PE")
print "This is what I really wanted ", a.listOfCourses
print a.__dict__


