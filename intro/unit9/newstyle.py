class NewStyle(object):
    '''this class contains no methods'''
    a, j = 0, 0.0 
    aDict = {'first':1,'second':2}
    aList = [44,55,77]

class Preferred(NewStyle):
    '''this class contains one method'''
    a, j = 1, 1.0
    def method1 (self):
	print "this is a class method"

n = NewStyle()
print n.a
print n.aList
print NewStyle.__dict__
print NewStyle.aList
p = Preferred()
print p.a
print p.aList
print Preferred.__dict__
