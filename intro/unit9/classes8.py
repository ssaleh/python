class Class8:					 
      count = 1
      firstName = "Joe"
      lastName = "Smith"
      listOfCourses = ["Accounting", "Math", "English", "Econ"]     
      def addCourseName(self, classname):
          self.listOfCourses.append(classname)
      def changeLastName(self,name):
            self.lastName = name
      def changeFirstName(self,name):
            self.firstName = name
      def printFullName(self):
            print self.firstName, "  ", self.lastName


a = Class8()
print "this is what you get when you try to print an object ", a

a.printFullName()
a.addCourseName("Woodshop")
a.changeLastName('Lunch')
a.changeFirstName("Eaton")
print "This is what I really wanted ", a.listOfCourses

