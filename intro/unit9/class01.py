'''Base class for people to be used for students and teachers'''
class People(object):
    '''Contains attributes common for people'''
    def __init__(self,firstName,lastName,id):
        self.firstName=firstName
        self.lastName=lastName
        self.id=id
        
class Student(People):
    schools = ['One Course Source, Inc.']
    courseList=[] 
    enrolled=True   
##    def __init__(self,firstName,lastName,id):
##        People.__init__(self,firstName,lastName,id)
##        self.courseList=[]
    def addCourse(self,course):
        self.courseList.append(course)
    def printCourses(self):
        for course in self.courseList:
            print course
    def delCourse(self,course):
        if course in self.courseList:
            self.courseList.remove(course)
            
students=[]
stud=Student('Keith','Wright',1)
print 'stud is enrolled?', stud.enrolled
stud.enrolled=False
print 'stud is enrolled?', stud.enrolled
print 'Student is enrolled?', Student.enrolled
print stud.__dict__
print Student.__dict__

stud.addCourse('Python')
Student.courseList.append('English')
print Student.courseList
stud.printCourses()
studette=Student('Patricia','Reynolds',2)
studette.printCourses()
studette.addCourse('Math')
studette.printCourses()
stud.printCourses()
