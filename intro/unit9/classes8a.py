def testFunction(objectToPrint):
      print "a function all alone", objectToPrint
 

class Class8:					 
      count = 1
      firstName = "Joe"
      lastName = "Smith"
      listOfCourses = ["Accounting", "Math", "English", "Econ"]     
      def addClass(self, classname):
          self.listOfCourses.append(classname)
          print "addClass method"
          def inside(number):     #is it a static class method?
                print self.firstName, number
          inside(4)     
      def printit(this,number):
            print this.lastName, number
      

a = Class8()            # creates an object instance
a.addClass("PE")        # invokes object's method on that instance
a.printit(3)
print
testFunction(a.lastName)

print "this is what you get when you try to print an object ", a
print "This is what I really wanted ", a.listOfCourses

