#classes14.py
from classes13 import Class13
age = 0
while not age:
    age = raw_input("Give me your age, anything non zero.")
 
name = raw_input("What is your name?")
if not name:
    name = "Herbert Hoover"
    print "Your name is now Herbert Hoover."

a = Class13(age,name)
print "Here is your information:"
a.printIt()

print "Now you have changed it to be:"
a.set2Values(44,"John Browne")
a.printIt()

print "Now you have changed to be:"
a.setNumber(100)
a.setString("Claire Older")
a.printIt()
