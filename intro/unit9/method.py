#method.py

class Student(object):					 
      count = 1
      firstName = "Joe"
      lastName = "Smith"
      listOfCourses = ["Accounting", "Math", "English", "Econ"]     
      def addCourseName(self, classname):
          self.listOfCourses.append(classname)
person = Student()
person.addCourseName("PE")
print person.listOfCourses
print "this is what you get when you try to print an object ", person
