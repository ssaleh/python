#StaticTest.py

class Show(object):
      def printArgs(x, y):
            print "This is a staticmethod with 2 arguments", x, y
      printArgs= staticmethod(printArgs)

Show.printArgs(110, 'one')
anObj = Show()
anObj.printArgs(110, 'one')
