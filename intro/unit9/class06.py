# class06.py
'''Class and static methods can be called with or without an instance

Static methods refer to the object attributes of the defining class'''
class StatusQuo(object):
    status = 'no change'
    def StaticStatus():
        return StatusQuo.status
    StaticStatus=staticmethod(StaticStatus)
    def ClassStatus(cls):
        return cls.status
    ClassStatus=classmethod(ClassStatus)
    
class StatusDerived(StatusQuo):
    status = 'dynamic'
    @classmethod 
    def ClsStatus(cls):
        return cls.status
    @staticmethod
    def StatStatus():
        return StatusDerived.status

print 'Class method StatusQuo.ClassStatus():', \
StatusQuo.ClassStatus()
print 'Static method StatusQuo.StaticStatus():', \
StatusQuo.StaticStatus()
s = StatusQuo()
print 'Class method s.ClassStatus():', s.ClassStatus()
print 'Static method s.StaticStatus():', s.StaticStatus()

print 'Class method StatusDerived.ClassStatus():', \
StatusDerived.ClassStatus()
print 'Static method StatusDerived.StaticStatus():', \
StatusDerived.StaticStatus()
d = StatusDerived()
print 'Class method d.ClassStatus():', d.ClassStatus()
print 'Static method d.StaticStatus():', d.StaticStatus()

print 'Class method StatusDerived.ClsStatus():', \
StatusDerived.ClsStatus()
print 'Static method StatusDerived.StatStatus():', \
StatusDerived.StatStatus()
print 'Class method d.ClsStatus():', d.ClsStatus()
print 'Static method d.StatStatus():',d.StatStatus()
