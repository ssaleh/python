class Class10:
    ''' this class has 3 data attributes '''
    def __init__(self):
        self.count = 1
        self.firstName = "Joe"
        self.lastName = "Smith"
        self.listOfCourses = ["Accounting", "Math", "English", "Econ"]
        
    def addClass(self, classname):
        self.listOfCourses.append(classname)
         
    def __str__(self):
        x =  "".join(self.listOfCourses)
        return "My name is " +  self.firstName + " " + self.lastName + " My courses are: "  + x 

a = Class10()
print "My last name is: ", a.lastName
print str(a)
