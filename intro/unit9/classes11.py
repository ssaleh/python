class Class11:
    ''' this class has several data and functional attributes '''
    def __init__(self):
        self.count = 1
        self.firstName = "Joe"
        self.lastName = "Smith"
        self.listOfCourses = ["Accounting", "Math", "English", "Econ"]
        
    def addClass(self, classname):
        self.listOfCourses.append(classname)
         
    def __str__(self):
        x =  " ".join(self.listOfCourses)
        return "My name is " +  self.firstName + " " + self.lastName + " My courses are: "  + x 


a = Class11()
a.r = "Freshman"
print "Using the object to see class attributes as a string"
print dir(a)

print "\nUsing the classname to see class attributes as a string"
print dir(Class11)

print "\nShow the instance namespace using vars "
print vars(a)

print "\nShow the instance namespace using __dict__"
print a.__dict__
