class Class12(object):
     "This is a test class"
     def __new__(self, num, aDescription):
          self.i = num
          self.str = aDescription

     def printIt(self):
	  '''I print the attributes of the object, but return nothing'''
	  print "Your age is ", self.i
	  print "Your name is ", self.str
	  return
     def return2Values(self):
	  '''I return two values of the class itself'''
          return self.i, self.str

     def set2Values(self, x, y):
	  '''I set the class values'''
          self.i = x
          self.str = y
          return        

 
