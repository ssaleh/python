#classes13.py
from classes12 import Class12
class Class13(Class12):
	"This is a derived class from Class12"
	def setNumber(self, x):
		'''I set the numeric value'''
		self.i = x
		return        

	def setString(self, y):
		'''I set the string value'''
		self.str = y
		return

